package com.topzi.chat.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DbHelper extends SQLiteOpenHelper {

    private String TAG = DbHelper.class.getSimpleName();

    private static final String DB_NAME = "abixdb";

    public static final String DB_TABLE_NAME = "users";
    public static final String DB_TABLE_CALL_OPPONENTS = "call_opponents";
    public static final String DB_TABLE_CALL = "call";

    public static final String DB_COLUMN_ID = "ID";
    public static final String DB_COLUMN_USER_FULL_NAME = "userFullName";
    public static final String DB_COLUMN_USER_LOGIN = "userLogin";
    public static final String DB_COLUMN_USER_ID = "userID";
    public static final String DB_COLUMN_USER_PASSWORD = "userPass";
    public static final String DB_COLUMN_USER_TAG = "userTag";
    public static final String DB_COLUMN_USER_PHONE = "userPhone";

    public static final String DB_COLUMN_CREATEDAT_TAG = "created_at";
    public static final String DB_COLUMN_SESSIONID_TAG = "session_id";
    public static final String DB_COLUMN_CALL_STATUS_TAG = "call_status";
    public static final String DB_COLUMN_CALL_TYPE_TAG = "call_type";
    public static final String DB_COLUMN_CALL_ID_TAG = "call_id";
    public static final String DB_COLUMN_OPP_CALL_ID_TAG = "opp_call_id";
    public static final String DB_COLUMN_CALL_DURATION_TAG = "call_duration";
    public DbHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "--- onCreate database ---");
        db.execSQL("create table " + DB_TABLE_NAME + " ("
                + DB_COLUMN_ID + " integer primary key autoincrement,"
                + DB_COLUMN_USER_ID + " integer,"
                + DB_COLUMN_USER_LOGIN + " text,"
                + DB_COLUMN_USER_PASSWORD + " text,"
                + DB_COLUMN_USER_FULL_NAME + " text,"
                + DB_COLUMN_USER_PHONE + " text,"
                + DB_COLUMN_USER_TAG + " text"
                + ");");
        db.execSQL("create table " + DB_TABLE_CALL + " ("
                + DB_COLUMN_SESSIONID_TAG + " text primary key,"
                + DB_COLUMN_CALL_ID_TAG + " text,"
                + DB_COLUMN_CREATEDAT_TAG + " text,"
                + DB_COLUMN_CALL_STATUS_TAG + " text,"
                + DB_COLUMN_CALL_DURATION_TAG + " text,"
                + DB_COLUMN_CALL_TYPE_TAG + " text"
                + ");");
        db.execSQL("create table " + DB_TABLE_CALL_OPPONENTS + " ("
                + DB_COLUMN_ID + " integer primary key autoincrement,"
                + DB_COLUMN_SESSIONID_TAG + " text,"
                + DB_COLUMN_CREATEDAT_TAG + " TEXT,"
                + DB_COLUMN_OPP_CALL_ID_TAG + " text"
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}