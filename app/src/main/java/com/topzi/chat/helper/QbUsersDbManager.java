package com.topzi.chat.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.model.QBUser;
import com.topzi.chat.activity.ApplicationClass;
import com.topzi.chat.model.ContactsData;
import com.topzi.chat.model.calls.CallsDataDto;
import com.topzi.chat.utils.Constants;
import com.topzi.chat.utils.GetSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class QbUsersDbManager {
    private static String TAG = "LLLLLL_Quickbox_DB: ";

    private static QbUsersDbManager instance;
    private Context mContext;

    private QbUsersDbManager(Context context) {
        this.mContext = context;
    }

    public static QbUsersDbManager getInstance(Context context) {
        if (instance == null) {
            instance = new QbUsersDbManager(context);
        }

        return instance;
    }

    public ArrayList<QBUser> getAllUsers() {
        ArrayList<QBUser> allUsers = new ArrayList<>();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DbHelper.DB_TABLE_NAME, null, null, null, null, null, null);

        if (c.moveToFirst()) {
            int userIdColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_ID);
            int userLoginColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_LOGIN);
            int userPassColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_PASSWORD);
            int userFullNameColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_FULL_NAME);
            int userTagColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_TAG);

            do {
                QBUser qbUser = new QBUser();

                qbUser.setFullName(c.getString(userFullNameColIndex));
                qbUser.setLogin(c.getString(userLoginColIndex));
                qbUser.setId(c.getInt(userIdColIndex));
                qbUser.setPassword(c.getString(userPassColIndex));

                StringifyArrayList<String> tags = new StringifyArrayList<>();
                tags.add(c.getString(userTagColIndex));
                qbUser.setTags(tags);

                allUsers.add(qbUser);
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return allUsers;
    }
    public QBUser getUserByLoginid(String userId){
        QBUser qbUser = null;
        String selectQuery = "SELECT  * FROM " + DbHelper.DB_TABLE_NAME+ " WHERE " + DbHelper.DB_COLUMN_USER_LOGIN + "="+userId;
        DbHelper dbHelper = new DbHelper(mContext);
        Cursor c = dbHelper.getWritableDatabase().rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            int userIdColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_ID);
            int userLoginColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_LOGIN);
            int userPassColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_PASSWORD);
            int userFullNameColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_FULL_NAME);
            int userTagColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_TAG);
            int userphone = c.getColumnIndex(DbHelper.DB_COLUMN_USER_PHONE);
            do {
                qbUser = new QBUser();
                qbUser.setFullName(c.getString(userFullNameColIndex));
                qbUser.setLogin(c.getString(userLoginColIndex));
                qbUser.setId(c.getInt(userIdColIndex));
                qbUser.setPassword(c.getString(userPassColIndex));
                qbUser.setPhone(c.getString(userphone));
                StringifyArrayList<String> tags = new StringifyArrayList<>();
                tags.add(c.getString(userTagColIndex).split(","));
                qbUser.setTags(tags);
                break;
            } while (c.moveToNext());
        }
        c.close();
        return qbUser;

    }

    public QBUser getUserById(Integer userId) {
        QBUser qbUser = null;
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DbHelper.DB_TABLE_NAME, null, null, null, null, null, null);

        if (c.moveToFirst()) {
            int userIdColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_ID);
            int userLoginColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_LOGIN);
            int userPassColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_PASSWORD);
            int userFullNameColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_FULL_NAME);
            int userTagColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_TAG);
            int userphone = c.getColumnIndex(DbHelper.DB_COLUMN_USER_PHONE);
            do {
                if (c.getInt(userIdColIndex) == userId) {
                    qbUser = new QBUser();
                    qbUser.setFullName(c.getString(userFullNameColIndex));
                    qbUser.setLogin(c.getString(userLoginColIndex));
                    qbUser.setId(c.getInt(userIdColIndex));
                    qbUser.setPassword(c.getString(userPassColIndex));
                    qbUser.setPhone(c.getString(userphone));
                    StringifyArrayList<String> tags = new StringifyArrayList<>();
                    tags.add(c.getString(userTagColIndex).split(","));
                    qbUser.setTags(tags);
                    break;
                }
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return qbUser;
    }

    public void saveAllUsers(ArrayList<QBUser> allUsers, boolean needRemoveOldData) {
        if (needRemoveOldData) {
            clearDB();
        }

        for (QBUser qbUser : allUsers) {
            saveUser(qbUser);
        }
        Log.d(TAG, "saveAllUsers");
    }

    public void saveUser(QBUser qbUser) {
        ContentValues cv = new ContentValues();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        cv.put(DbHelper.DB_COLUMN_USER_FULL_NAME, qbUser.getFullName());
        cv.put(DbHelper.DB_COLUMN_USER_LOGIN, qbUser.getLogin());
        cv.put(DbHelper.DB_COLUMN_USER_ID, qbUser.getId());
        cv.put(DbHelper.DB_COLUMN_USER_PASSWORD, qbUser.getPassword());
        cv.put(DbHelper.DB_COLUMN_USER_PHONE, qbUser.getPhone());
        cv.put(DbHelper.DB_COLUMN_USER_TAG, qbUser.getTags().getItemsAsString());

        if(isUserExist(db,qbUser.getLogin())){
            db.update(DbHelper.DB_TABLE_NAME, cv, DbHelper.DB_COLUMN_USER_LOGIN + " =? ",
                    new String[]{qbUser.getLogin()});
        }else
            db.insert(DbHelper.DB_TABLE_NAME, null, cv);
        dbHelper.close();
    }
    public boolean isUserExist(SQLiteDatabase db, String userId) {
        long line = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + DbHelper.DB_TABLE_NAME + " WHERE " + DbHelper.DB_COLUMN_USER_LOGIN + "=?",
                new String[]{userId});
        return line > 0;
    }

    public void clearDB() {
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DbHelper.DB_TABLE_NAME, null, null);
        dbHelper.close();
    }

    public ArrayList<QBUser> getUsersByIds(List<Integer> usersIds) {
        ArrayList<QBUser> qbUsers = new ArrayList<>();

        for (Integer userId : usersIds) {
            if (getUserById(userId) != null) {
                qbUsers.add(getUserById(userId));
            }
        }

        return qbUsers;
    }
    public void addRecentCall(String sessionId, String caller_id, String createdDate,
                              String call_status,String duration,String callType,List<Integer> opponents) {
        try {
            DbHelper dbHelper = new DbHelper(mContext);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            boolean exists = iscallExist(db, sessionId);
            ContentValues values = new ContentValues();
            values.put(DbHelper.DB_COLUMN_SESSIONID_TAG, sessionId);
            values.put(DbHelper.DB_COLUMN_CALL_ID_TAG, caller_id);
            values.put(DbHelper.DB_COLUMN_CALL_STATUS_TAG, call_status);
            values.put(DbHelper.DB_COLUMN_CALL_DURATION_TAG, duration);
            values.put(DbHelper.DB_COLUMN_CALL_TYPE_TAG, callType);
            if(exists){
                db.update(DbHelper.DB_TABLE_CALL, values, DbHelper.DB_COLUMN_SESSIONID_TAG + " =? ",
                        new String[]{sessionId});
            }else{
                values.put(DbHelper.DB_COLUMN_CREATEDAT_TAG, createdDate);
                dbHelper.getWritableDatabase().insertWithOnConflict(DbHelper.DB_TABLE_CALL, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }

            for(Integer opponentid : opponents){
                ContentValues valuesopp = new ContentValues();
                valuesopp.put(DbHelper.DB_COLUMN_SESSIONID_TAG, sessionId);
                valuesopp.put(DbHelper.DB_COLUMN_OPP_CALL_ID_TAG, ""+opponentid);
                if(isopponentExist(db,sessionId,""+opponentid)){
                    db.update(DbHelper.DB_TABLE_CALL_OPPONENTS, values, DbHelper.DB_COLUMN_SESSIONID_TAG + " =? "+" AND "+DbHelper.DB_COLUMN_OPP_CALL_ID_TAG+"=?",
                            new String[]{sessionId,""+opponentid});
                } else{
                    valuesopp.put(DbHelper.DB_COLUMN_CREATEDAT_TAG, createdDate);
                    dbHelper.getWritableDatabase().insertWithOnConflict(DbHelper.DB_TABLE_CALL_OPPONENTS, null, valuesopp, SQLiteDatabase.CONFLICT_REPLACE);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public boolean iscallExist(SQLiteDatabase db, String sessionid) {
        long line = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + DbHelper.DB_TABLE_CALL + " WHERE " + DbHelper.DB_COLUMN_SESSIONID_TAG + "=?",
                new String[]{sessionid});
        return line > 0;
    }
    public boolean isopponentExist(SQLiteDatabase db, String sessionid,String opponentId) {
        long line = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + DbHelper.DB_TABLE_CALL_OPPONENTS + " WHERE "
                        + DbHelper.DB_COLUMN_SESSIONID_TAG + "=?"+" AND "+DbHelper.DB_COLUMN_OPP_CALL_ID_TAG+"=?",
                new String[]{sessionid,opponentId});
        return line > 0;
    }

    public List<CallsDataDto> getAllcalls(){
        String selectQuery = "SELECT  * FROM " + DbHelper.DB_TABLE_CALL+ " ORDER BY " + DbHelper.DB_COLUMN_CREATEDAT_TAG + " DESC";
        DbHelper dbHelper = new DbHelper(mContext);
        Cursor cursor = dbHelper.getWritableDatabase().rawQuery(selectQuery, null);
        List<CallsDataDto> callsList = new ArrayList<>();
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CallsDataDto results = new CallsDataDto();
                String sessionId= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_SESSIONID_TAG));
                String caller_id= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_CALL_ID_TAG));
                String createdDate= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_CREATEDAT_TAG));
                String call_status= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_CALL_STATUS_TAG));
                String duration= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_CALL_DURATION_TAG));
                String callType= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_CALL_TYPE_TAG));
                List<String> opponents=getAllOpponents(sessionId);
                results.sessionId=sessionId;
                results.callerId=caller_id;
                results.createdDate=createdDate;
                results.callStatus=call_status;
                results.callDuration=duration;
                results.callType=callType;
                results.opponetIds=opponents;
                callsList.add(results);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return callsList;
    }
    private List<String> getAllOpponents(String sessionId){
        String selectQuery = "SELECT  * FROM " + DbHelper.DB_TABLE_CALL_OPPONENTS + " WHERE " + DbHelper.DB_COLUMN_SESSIONID_TAG + "='" + sessionId + "'";
        List<String> oppnesList = new ArrayList<>();
        DbHelper dbHelper = new DbHelper(mContext);
        Cursor cursor = dbHelper.getWritableDatabase().rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                String opponetId = cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_OPP_CALL_ID_TAG));
                oppnesList.add(opponetId);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return oppnesList;
    }

    public List<CallsDataDto> getcallDetails(String sessionId){
        String selectQuery = "SELECT  * FROM " + DbHelper.DB_TABLE_CALL + " WHERE " + DbHelper.DB_COLUMN_SESSIONID_TAG + "='" + sessionId + "'";
        DbHelper dbHelper = new DbHelper(mContext);
        Cursor cursor = dbHelper.getWritableDatabase().rawQuery(selectQuery, null);
        List<CallsDataDto> callsList = new ArrayList<>();
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CallsDataDto results = new CallsDataDto();
                String session_id= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_SESSIONID_TAG));
                String caller_id= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_CALL_ID_TAG));
                String createdDate= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_CREATEDAT_TAG));
                String call_status= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_CALL_STATUS_TAG));
                String duration= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_CALL_DURATION_TAG));
                String callType= cursor.getString(cursor.getColumnIndex(DbHelper.DB_COLUMN_CALL_TYPE_TAG));
                List<String> opponents=getAllOpponents(sessionId);
                results.sessionId=sessionId;
                results.callerId=caller_id;
                results.createdDate=createdDate;
                results.callStatus=call_status;
                results.callDuration=duration;
                results.callType=callType;
                results.opponetIds=opponents;
//                if(session_id.equals(sessionId))
                    callsList.add(results);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return callsList;
    }
}