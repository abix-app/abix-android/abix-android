package com.topzi.chat.fragment;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.AppRTCAudioManager;
import com.topzi.chat.R;
import com.topzi.chat.activity.ApplicationClass;
import com.topzi.chat.activity.CallActivity;
import com.topzi.chat.helper.DatabaseHandler;
import com.topzi.chat.utils.CollectionsUtils;
import com.topzi.chat.utils.Constants;
import com.topzi.chat.utils.SharedPrefsHelper;
import com.topzi.chat.utils.UiUtils;

import java.util.ArrayList;


public class AudioConversationFragment extends BaseConversationFragment implements CallActivity.OnChangeAudioDevice {
    private static final String TAG = AudioConversationFragment.class.getSimpleName();

    public static final String SPEAKER_ENABLED = "is_speaker_enabled";

    private ToggleButton audioSwitchToggleButton;
//    private TextView alsoOnCallText;
//    private TextView firstOpponentNameTextView;
//    private TextView otherOpponentsTextView;
    private ToggleButton cameraToggle;
    private ImageView iv_oppBg;
    LinearLayout ll_bottom_call;


    @Override
    public void onStart() {
        super.onStart();
        if (conversationFragmentCallback != null) {
            conversationFragmentCallback.addOnChangeAudioDeviceListener(this);
        }
    }

    @Override
    protected void configureOutgoingScreen() {
//        outgoingOpponentsRelativeLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
        allOpponentsTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        ringingTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
    }

    @Override
    protected void configureToolbar() {
        toolbar.setVisibility(View.GONE);
        toolbar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
        toolbar.setTitleTextColor(ContextCompat.getColor(getActivity(), R.color.toolbar_title_color));
        toolbar.setSubtitleTextColor(ContextCompat.getColor(getActivity(), R.color.toolbar_subtitle_color));
    }

    @Override
    protected void configureActionBar() {
        actionBar.setSubtitle(String.format(getString(R.string.subtitle_text_logged_in_as), currentUser.getFullName()));
    }

    @Override
    protected void initViews(View view) {
        super.initViews(view);
        if (view == null) {
            return;
        }
//        timerCallText = view.findViewById(R.id.timer_call);
        cameraToggle = (ToggleButton) view.findViewById(R.id.toggle_camera);
        cameraToggle.setVisibility(View.GONE);
        ll_bottom_call=view.findViewById(R.id.ll_bottom_call);
        ll_bottom_call.setBackgroundColor( getContext().getResources().getColor(R.color.gradient_end));
//        ImageView firstOpponentAvatarImageView = (ImageView) view.findViewById(R.id.image_caller_avatar);
//        firstOpponentAvatarImageView.setBackgroundDrawable(UiUtils.getColorCircleDrawable(opponents.get(0).getId()));
        iv_oppBg=view.findViewById(R.id.iv_oppBg);

//        alsoOnCallText = (TextView) view.findViewById(R.id.text_also_on_call);
//        setVisibilityAlsoOnCallTextView();

//        firstOpponentNameTextView = (TextView) view.findViewById(R.id.text_caller_name);
//        firstOpponentNameTextView.setText(opponents.get(0).getFullName());

//        otherOpponentsTextView = (TextView) view.findViewById(R.id.text_other_inc_users);
//        otherOpponentsTextView.setText(getOtherOpponentsNames());

        audioSwitchToggleButton = (ToggleButton) view.findViewById(R.id.toggle_speaker);
        audioSwitchToggleButton.setVisibility(View.VISIBLE);
        audioSwitchToggleButton.setChecked(SharedPrefsHelper.getInstance().get(SPEAKER_ENABLED, true));

        String imgUrl=Constants.BASE_URL+dbhelper.getContactDetail(Integer.parseInt(opponents.get(0).getLogin())).user_image;
        Glide.with(getActivity()).load(imgUrl).thumbnail(0.5f)
                .into(iv_oppBg);
//        Glide.with(getActivity()).load(imgUrl).thumbnail(0.5f)
//                .into(firstOpponentAvatarImageView);
        actionButtonsEnabled(false);

        if (conversationFragmentCallback != null && conversationFragmentCallback.isCallState()) {
            onCallStarted();
        }
    }

    private void setVisibilityAlsoOnCallTextView() {
        if (opponents.size() < 2) {
//            alsoOnCallText.setVisibility(View.INVISIBLE);
        }
    }

    private String getOtherOpponentsNames() {
        ArrayList<QBUser> otherOpponents = new ArrayList<>();
        otherOpponents.addAll(opponents);
        otherOpponents.remove(0);
        return CollectionsUtils.makeStringFromUsersFullNames(otherOpponents);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (conversationFragmentCallback != null) {
            conversationFragmentCallback.removeOnChangeAudioDeviceListener(this);
        }
    }

    @Override
    protected void initButtonsListener() {
        super.initButtonsListener();
        audioSwitchToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPrefsHelper.getInstance().save(SPEAKER_ENABLED, isChecked);
                if (conversationFragmentCallback != null) {
                    conversationFragmentCallback.onSwitchAudio();
                }
            }
        });
    }

    @Override
    protected void actionButtonsEnabled(boolean enabled) {
        super.actionButtonsEnabled(enabled);
        audioSwitchToggleButton.setActivated(enabled);
    }

    @Override
    int getFragmentLayout() {
        return R.layout.fragment_audio_conversation;
    }

    @Override
    public void onOpponentsListUpdated(ArrayList<QBUser> newUsers) {
        super.onOpponentsListUpdated(newUsers);
//        firstOpponentNameTextView.setText(opponents.get(0).getFullName());
//        otherOpponentsTextView.setText(getOtherOpponentsNames());
//        String imgUrl=dbhelper.getContactDetail(Integer.parseInt(newUsers.get(0).getLogin())).user_image;
//        Glide.with(getActivity()).load(imgUrl).thumbnail(0.5f)
//                .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.change_camera)
//                        .error(R.drawable.change_camera)
//                        .override(ApplicationClass.dpToPx(getContext(), 70)))
//                .into(iv_oppBg);
    }

    @Override
    public void onCallTimeUpdate(String time) {
        ringingTextView.setText(time);
    }

    @Override
    public void audioDeviceChanged(AppRTCAudioManager.AudioDevice newAudioDevice) {
        audioSwitchToggleButton.setChecked(newAudioDevice != AppRTCAudioManager.AudioDevice.SPEAKER_PHONE);
    }
}