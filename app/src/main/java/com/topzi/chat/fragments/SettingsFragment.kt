package com.topzi.chat.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.topzi.chat.R
import com.topzi.chat.activity.*
import com.topzi.chat.helper.DatabaseHandler
import com.topzi.chat.model.ContactsData
import com.topzi.chat.utils.Constants
import com.topzi.chat.utils.GetSet
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_dashboard.view.*

class SettingsFragment : Fragment(){
    var dbhelper: DatabaseHandler? = null
    var rootview:View ? =null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootview=inflater.inflate(R.layout.activity_dashboard, container, false)
        this.rootview=rootview
        dbhelper = DatabaseHandler.getInstance(context)
        intView(rootview)
        initListeners()
        return rootview
    }
    fun intView(v:View){
        v.toolbar.visibility=View.GONE
        v.tv_name.setText(GetSet.getUserName())
        Glide.with(this).load(Constants.USER_IMG_PATH + GetSet.getImageUrl())
                .apply(RequestOptions().placeholder(R.drawable.person).error(R.drawable.person))
                .into(v.iv_ProfileImg)
        val result: ContactsData.Result? = dbhelper?.getContactDetail(GetSet.getUserId().toInt())
        DialogActivity.setAboutUs(result,v.tv_status)
    }
    fun initListeners(){
        rootview?.backbtn?.setOnClickListener { activity?.onBackPressed() }
        rootview?.lay_top?.setOnClickListener {
            val p = Intent(context, ProfileActivity::class.java)
            p.putExtra(Constants.TAG_USER_ID, GetSet.getUserId())
            startActivity(p)
        }
        rootview?.ll_acount_settings?.setOnClickListener {
            var account = Intent(context, AccountActivity::class.java)
            startActivity(account)
        }
        rootview?.ll_chat?.setOnClickListener {
            val chatsettings = Intent(context, ChatSettingsActivity::class.java)
            startActivity(chatsettings)
        }
        rootview?.ll_notification?.setOnClickListener {
            val notificationin = Intent(context, NotifictionActivity::class.java)
            startActivity(notificationin)
        }
        rootview?.ll_storage?.setOnClickListener {
            val dataIntent = Intent(context, DataStorage::class.java)
            startActivity(dataIntent)
        }
        rootview?.ll_stared?.setOnClickListener {
            val staredMsgs = Intent(context, StarredMessagesActivity::class.java)
            startActivity(staredMsgs)
        }
        rootview?.ll_invite?.setOnClickListener {
            val g = Intent(Intent.ACTION_SEND)
            g.type = "text/plain"
            g.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_message) + "https://play.google.com/store/apps/details?id=" +
                    context?.packageName)
            startActivity(Intent.createChooser(g, "Share"))
        }
        rootview?.ll_help?.setOnClickListener {
            val help = Intent(context, BaseHelpActivity::class.java)
            help.putExtra("fragment_tag", "tag_help")
            startActivity(help)
        }
    }
}