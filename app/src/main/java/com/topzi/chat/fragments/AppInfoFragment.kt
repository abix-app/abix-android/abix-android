package com.abix.chat.fragments

import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.topzi.chat.R
import kotlinx.android.synthetic.main.fragment_app_info.view.*


class AppInfoFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootview=inflater.inflate(R.layout.fragment_app_info, container, false)
        intView(rootview)
        return rootview
    }
    fun intView(rootview: View) {
        try {
            val pInfo = context?.packageManager?.getPackageInfo(context?.packageName, 0)
            val version = pInfo?.versionName
            rootview.tv_version.text="Version : "+version
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }


}
