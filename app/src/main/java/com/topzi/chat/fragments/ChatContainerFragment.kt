package com.topzi.chat.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.topzi.chat.R
import com.topzi.chat.activity.*
import com.topzi.chat.helper.DatabaseHandler
import com.topzi.chat.helper.SocketConnection
import com.topzi.chat.helper.SocketConnection.OnUpdateTabIndication
import com.topzi.chat.utils.Constants
import com.topzi.chat.utils.GetSet
import kotlinx.android.synthetic.main.fragment_chat_container.view.*
import java.util.*

class ChatContainerFragment: Fragment(), TabLayout.OnTabSelectedListener, OnUpdateTabIndication {
    private var pref: SharedPreferences? = null
    var chatFragment: ChatFragment? = null
    var groupFragment: GroupFragment? = null
    private var pin = ""
    private var editor: SharedPreferences.Editor? = null
    var tabLayout: TabLayout? = null
    var adapter: ViewPagerAdapter? = null
    var rootview: View?=null
    var dbhelper: DatabaseHandler? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootview=inflater.inflate(R.layout.fragment_chat_container, container, false)
        //        View header = navigationView.getHeaderView(0);
        //initPushManager();
        this.rootview=rootview
        pref = context?.getSharedPreferences("SavedPref", Context.MODE_PRIVATE)
        dbhelper = DatabaseHandler.getInstance(context)
        SocketConnection.getInstance(context).setOnUpdateTabIndication(this)
        editor = pref?.edit()
        intView(rootview)
        updateTabIndication()
        return rootview
    }
    fun intView(rootview: View) {
        chatFragment= ChatFragment()
        groupFragment= GroupFragment()
        tabLayout=rootview.tabs
        tabLayout?.addOnTabSelectedListener(this)
        setupViewPager(rootview.viewpager)
        rootview.fabSearch.setOnLongClickListener{
            if (pref?.getString("hideChatPin", "") == "") {
                setHideChatsPin()
            } else {
                if (!Constants.isShow) checkHideChatsPin() else chatFragment?.removeHideData()
            }
            return@setOnLongClickListener true
        }
        rootview.fabSearch.setOnClickListener {
            if (rootview.tabs != null && rootview.tabs.getSelectedTabPosition() == 0) {
                if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(context as Activity, arrayOf(Manifest.permission.READ_CONTACTS), 101)
                } else {
                    val s = Intent(context, SelectContact::class.java)
                    s.putExtra(Constants.TAG_USER_ID, GetSet.getUserId())
                    startActivity(s)
                }
            }
        }

//        if (context.getIntent().getStringArrayExtra(ImageEditor.EXTRA_EDITED_PATH) != null) {
//            rootview.viewpager.setCurrentItem(2)
//        } else {
//            if (rootview.viewpager != null && getIntent().getStringExtra(Constants.IS_FROM) != null) {
//                if (getIntent().getStringExtra(Constants.IS_FROM).equals("group", ignoreCase = true)) {
//                    rootview.viewpager.setCurrentItem(1)
//                } else if (getIntent().getStringExtra(Constants.IS_FROM).equals("channel", ignoreCase = true)) {
//                    rootview.viewpager.setCurrentItem(2)
//                }
//            }
//        }

        rootview.viewpager.setOffscreenPageLimit(1)
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {
        adapter?.setUnSelectView(context!!, tabLayout!!, tab?.position!!)
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
//        MainActivity.bottomNavigationView.setOnNavigationItemReselectedListener {  }
        adapter?.setOnSelectView(context!!, tabLayout!!, tab?.position!!)
        if (tab?.position == 0) {
            rootview?.fab?.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.home_page_chat))
            rootview?.fabSearch?.setOnClickListener(View.OnClickListener {
                if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.READ_CONTACTS), 101)
                } else {
                    val s = Intent(context, SelectContact::class.java)
                    s.putExtra(Constants.TAG_USER_ID, GetSet.getUserId())
                    startActivity(s)
                }
            })
        } else if (tab?.position == 1) {
            rootview?.fab?.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.floating_group))
            rootview?.fabSearch?.setOnClickListener(View.OnClickListener {
                val s = Intent(context, NewGroupActivity::class.java)
                s.putExtra(Constants.TAG_USER_ID, GetSet.getUserId())
                startActivity(s)
            })
        }
    }

    private fun setHideChatsPin() {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialoge_set_pin)
        dialog.window?.setLayout(resources.displayMetrics.widthPixels * 90 / 100, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        val title = dialog.findViewById<TextView>(R.id.title)
        val etPin = dialog.findViewById<EditText>(R.id.etPin)
        val yes = dialog.findViewById<TextView>(R.id.yes)
        val no = dialog.findViewById<TextView>(R.id.no)
        etPin.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (etPin.text.length == 4) {
                    if (yes.text == "Confirm") {
                        if (etPin.text.toString().trim { it <= ' ' } != pin) {
                            etPin.error = "Invalid Pin"
                        }
                    }
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        no.visibility = View.VISIBLE
        yes.setOnClickListener {
            Log.e("LLLL_Confirm: ", etPin.text.length.toString())
            if (etPin.text.length == 4) {
                if (yes.text == "Confirm") {
                    if (etPin.text.toString().trim { it <= ' ' } != pin) {
                        etPin.error = "Invalid Pin"
                    } else {
                        editor?.putString("hideChatPin", pin)
                        editor?.commit()
                        dialog.dismiss()
                    }
                } else {
                    pin = etPin.text.toString().trim { it <= ' ' }
                    etPin.setText("")
                    title.text = "Confirm pin for hide chat"
                    yes.text = "Confirm"
                }
            } else {
                etPin.error = "Please Enter 4Digit Pin"
            }
        }
        no.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }
    private fun checkHideChatsPin() {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialoge_set_pin)
        dialog.window?.setLayout(resources.displayMetrics.widthPixels * 90 / 100, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        val etPin = dialog.findViewById<EditText>(R.id.etPin)
        val title = dialog.findViewById<TextView>(R.id.title)
        val yes = dialog.findViewById<TextView>(R.id.yes)
        val no = dialog.findViewById<TextView>(R.id.no)
        title.text = "Enter the pin"
        no.visibility = View.VISIBLE
        yes.setOnClickListener {
            if (etPin.text.toString().trim { it <= ' ' } != pref?.getString("hideChatPin", "")) {
                etPin.error = "Invalid Pin"
            } else {
                dialog.dismiss()
                chatFragment?.addHideData()
//                    Intent intent = new Intent(MainActivity.this, HideChatActivity.class);
//                    startActivity(intent);
//                    finish();
            }
        }
        no.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }
    private fun setupViewPager(viewPager: ViewPager) {
        adapter = ViewPagerAdapter(childFragmentManager, context!!)
        chatFragment = ChatFragment()
        adapter?.addFragment(chatFragment!!, getString(R.string.chat))
        adapter?.addFragment(groupFragment!!, getString(R.string.group))
        viewPager.adapter = adapter
        tabLayout?.setupWithViewPager(viewPager)
        // Iterate over all tabs and set the custom view
        for (i in 0 until tabLayout?.tabCount!!) {
            val tab = tabLayout?.getTabAt(i)
            tab?.setCustomView(adapter?.getTabView(i, context))
        }
//        if (getIntent().getStringArrayExtra(ImageEditor.EXTRA_EDITED_PATH) != null) {
//            adapter.setOnSelectView(this, tabLayout, 2)
//        } else adapter.setOnSelectView(this, tabLayout, 0)
    }

    @SuppressLint("WrongConstant")
    class ViewPagerAdapter(manager: FragmentManager?, var context: Context) : FragmentPagerAdapter(manager!!, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private val mFragmentList: MutableList<Fragment> = ArrayList()
        private val mFragmentTitleList: MutableList<String> = ArrayList()
        fun getTabView(position: Int, context: Context?): View {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            val v = LayoutInflater.from(context).inflate(R.layout.tab_layout, null)
            val tabName = v.findViewById<View>(R.id.tabName) as TextView
            tabName.text = mFragmentTitleList[position]
            tabName.isAllCaps = true
            // ImageView indication = (ImageView) v.findViewById(R.id.indication);
            return v
        }

        fun setOnSelectView(mContext: Context, tabLayout: TabLayout, position: Int) {
            val tab = tabLayout.getTabAt(position)
            val selected = tab?.customView
            val tabName = selected?.findViewById<TextView>(R.id.tabName)
            tabName?.setTextColor(mContext.resources.getColor(R.color.white))
        }

        fun setUnSelectView(mContext: Context, tabLayout: TabLayout, position: Int) {
            val tab = tabLayout.getTabAt(position)
            val selected = tab?.customView
            val iv_text = selected?.findViewById<TextView>(R.id.tabName)
            iv_text?.setTextColor(mContext.resources.getColor(R.color.light_end))
        }

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    private fun updateTabIndication() {
        for (i in 0 until tabLayout?.tabCount!!) {
            val tab = tabLayout?.getTabAt(i)
            val selected = tab?.customView
            val indication = selected?.findViewById<TextView>(R.id.indication)
            val iv_indication = selected?.findViewById<ImageView>(R.id.iv_indication)
            if (i == 0) {
                if (dbhelper?.isRecentChatIndicationExist()!! > 0) {
                    indication?.visibility = View.VISIBLE
                    iv_indication?.visibility = View.GONE
                    indication?.text = "" + dbhelper?.isRecentChatIndicationExist()
                } else {
                    indication?.visibility = View.GONE
                    iv_indication?.visibility = View.GONE
                }
            } else if (i == 1) {
                if (dbhelper?.isRecentGroupIndicationExist() !!> 0) {
                    indication?.visibility = View.VISIBLE
                    iv_indication?.visibility = View.GONE
                    indication?.text = "" + dbhelper?.isRecentGroupIndicationExist()
                } else {
                    indication?.visibility = View.GONE
                    iv_indication?.visibility = View.GONE
                }
            } else if (i == 2) {
                if (dbhelper?.isRecentChannelIndicationExist() !!> 0) {
                    indication?.visibility = View.VISIBLE
                    iv_indication?.visibility = View.VISIBLE
                    indication?.visibility = View.GONE
                } else {
                    indication?.visibility = View.GONE
                    iv_indication?.visibility = View.GONE
                }
            } else indication?.visibility = View.GONE
        }
    }

    override fun onResume() {
        SocketConnection.getInstance(context).setOnUpdateTabIndication(this)
        updateTabIndication()
        super.onResume()
    }

    override fun updateIndication() {
        updateTabIndication()
    }
}