package com.topzi.chat.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;

import com.topzi.chat.R;
import com.topzi.chat.helper.NetworkUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class MyUtils {
    public static final String DATE_WITH_TIME_FORMAT = "yyyy-MM-dd kk:mm:ss";
    public static final boolean D = false;
    public static String isNetworkConnected(Context context) {
        return NetworkUtil.getConnectivityStatusString(context);
    }
    public static String getSearchTextquery(String colName,String searchtext){
        String searcharr[]=searchtext.split(" ");
        String query="";
        for (int i = 0; i < searcharr.length; i++) {
            query+=colName+" LIKE "+"'"+searcharr[i]+"%'";
            if(i<searcharr.length-1)
                query+=" OR " ;
        }
        return query;
    }
    public static String getFileName(String url) {
        String imgSplit = url;
        int endIndex = imgSplit.lastIndexOf("/");
        if (endIndex != -1) {
            imgSplit = imgSplit.substring(endIndex + 1);
        }
        return imgSplit;
    }

    public static String getFormattedDateTime(Context context, long smsTimeInMilis) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMilis /** 1000L*/);

        Calendar now = Calendar.getInstance();

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "EEEE, MMMM d, h:mm aa";
        final long HOURS = 60 * 60 * 60;
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return context.getString(R.string.today) + " " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
            return context.getString(R.string.yesterday) + " " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        } else {
            return DateFormat.format("MMMM dd yyyy, h:mm aa", smsTime).toString();
        }
    }
    public static boolean isStatusExpired(Context context, long smsTimeInMilis) {
        Log.v("chatTime1", "chatTimetest=");
        boolean isExpired=false;
        Calendar smsTime = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        smsTime.setTimeInMillis(smsTimeInMilis);
        Log.v("chatTime2", "chatTimetest=" + smsTime.get(Calendar.DATE));
        Calendar now = Calendar.getInstance();

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "EEEE, MMMM d, h:mm aa";
        final long HOURS = 60 * 60 * 60;
        Log.v("chatTime3", "chatTimetest=" + smsTime.get(Calendar.DATE));

        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            Log.v("chatTime4", "chatTimetest=" + smsTime.get(Calendar.DATE));

            return false;
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
            return true;
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            return true;
        } else {
            return true;
        }
    }
    public static synchronized Date stringToDate(String dateString, String format) {
        Date date = null;
        @SuppressLint("SimpleDateFormat")
        java.text.DateFormat df = new SimpleDateFormat(format);
        try {
            date = df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    public static String convertUTCTime(String sDate) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat(DATE_WITH_TIME_FORMAT);
        String formattedDate = null;
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(sDate);
            df.setTimeZone(TimeZone.getDefault());
            formattedDate = df.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }
    public static String getcurrentTime(){
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
        Date currentLocalTime = cal.getTime();
        SimpleDateFormat date = new SimpleDateFormat(DATE_WITH_TIME_FORMAT);
        // you can get seconds by adding  "...:ss" to it
        date.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));

        String localTime = date.format(currentLocalTime);
        return localTime;
    }
    public static String getDate(long date){
        SimpleDateFormat sdf = new SimpleDateFormat(MyUtils.DATE_WITH_TIME_FORMAT);
        Date resultdate = new Date(date);
        return sdf.format(resultdate);
    }

    public static Long convertDateStringToMs(String date,String pattern){
        Long milliseconds=0L;
        SimpleDateFormat f = new SimpleDateFormat(pattern);
        try {
            Date d = f.parse(date);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return milliseconds;
    }

    public static boolean isOnline(Activity activity) {
        NetworkInfo networkInfo = ((ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnectedOrConnecting() && networkInfo.isAvailable()) ? true : D;
    }
    public static int getRandomMaterialColor(Context context,String typeColor) {
        int returnColor = Color.GRAY;
        int arrayId = context.getResources().getIdentifier("mdcolor_" + typeColor, "array", context.getPackageName());

        if (arrayId != 0) {
            TypedArray colors = context.getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }
}
