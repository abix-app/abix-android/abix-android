package com.topzi.chat.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacksImpl;
import com.topzi.chat.activity.CallActivity;
import com.topzi.chat.helper.DatabaseHandler;
import com.topzi.chat.helper.QbUsersDbManager;
import com.topzi.chat.model.ContactsData;
import com.topzi.chat.model.UserProfileModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.topzi.chat.utils.Constants.TAG_NOBODY;


public class WebRtcSessionManager extends QBRTCClientSessionCallbacksImpl {
    private static final String TAG = WebRtcSessionManager.class.getSimpleName();
    private QbUsersDbManager dbManager ;
    private static WebRtcSessionManager instance;
    private Context context;
    DatabaseHandler dbhelper;
    private static QBRTCSession currentSession;


    private WebRtcSessionManager(Context context) {
        this.context = context;
        dbhelper = DatabaseHandler.getInstance(context);
    }

    public static WebRtcSessionManager getInstance(Context context) {
        if (instance == null) {
            instance = new WebRtcSessionManager(context);
        }

        return instance;
    }

    public QBRTCSession getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(QBRTCSession qbCurrentSession) {
        currentSession = qbCurrentSession;
    }

    @Override
    public void onReceiveNewSession(QBRTCSession session) {
        Log.d(TAG, "onReceiveNewSession to WebRtcSessionManager");
        if (currentSession == null && session != null) {
            setCurrentSession(session);
            CallActivity.start(context, true);

            dbManager = QbUsersDbManager.getInstance(context);
            String sessionId=session.getSessionDescription().getSessionId();
            Integer callerId=session.getSessionDescription().getCallerID();
            List<Integer> opponents=session.getSessionDescription().getOpponents();
            String type= QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO == session.getConferenceType()?"video":"audio";
            String callType="incoming"+type;
            //add to contacts
            checkAndAddAnonimousUsers(callerId);
            for(Integer id:opponents){
                checkAndAddAnonimousUsers(id);
            }
            dbManager.addRecentCall(sessionId,""+callerId, ""+System.currentTimeMillis(),"CONNECTING","0.0",callType,opponents);
        }
    }

    private void checkAndAddAnonimousUsers(Integer usersId){
        QBUser user=dbManager.getUserById(usersId);

        if(user!=null)
            if(!dbhelper.isUserExist(user.getLogin())){
                if(!user.getLogin().equals(GetSet.getUserId()))
                    dbhelper.addContactDetails(user.getLogin(), user.getFullName(), user.getPhone(), "", "", "",
                            "", "", "", "false");
            }else{
                ContactsData.Result results = dbhelper.getContactDetail(Integer.parseInt(user.getLogin()));
                dbhelper.addContactDetails(user.getLogin(), user.getFullName(), user.getPhone(),
                        results.country_code, results.user_image, results.privacy_about,
                        results.privacy_last_seen, results.privacy_profile_image, results.about, results.contactstatus);
                Log.e("usercontact",new Gson().toJson(dbhelper.getContactDetail(Integer.parseInt(user.getLogin()))));
        }
    }

    void getuserprofile(String userId) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserProfileModel> call3 = apiInterface.getuserprofile(userId);
        call3.enqueue(new Callback<UserProfileModel>() {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                try {
                    UserProfileModel userdata = response.body();
                    if (userdata != null && userdata.getSTATUS().equals("true")) {
                        for (UserProfileModel.RESULT result:userdata.getRESULT()){
                            if (!dbhelper.isUserExist(result.getId())) {
                                dbhelper.addContactDetails(result.getId(), result.getUserName(), result.getPhoneNo(), result.getCountryCode(), result.getUserImage(),
                                        result.getPrivacyAbout(), result.getPrivacyLastSeen(), result.getPrivacyProfileImage(), result.getAbout(), "false");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {
                Log.v("getuserprofile Failed", "TEST" + t.getMessage());
                call.cancel();
            }
        });

    }

    @Override
    public void onSessionClosed(QBRTCSession session) {
        Log.d(TAG, "onSessionClosed WebRtcSessionManager");

        if (session.equals(getCurrentSession())) {
            setCurrentSession(null);
        }
    }
}