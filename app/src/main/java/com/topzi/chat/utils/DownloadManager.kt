package com.topzi.chat.utils

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.topzi.chat.R
import com.topzi.chat.external.ProgressWheel
import com.topzi.chat.helper.DatabaseHandler
import com.topzi.chat.helper.StorageManager
import com.topzi.chat.model.DataStorageModel
import java.io.File

/**
 * Created on 27/6/18.
 */
class DownloadManager //        extends AsyncTask<String, String, Bitmap>
(private val context: Context, url: String, from: String?, progressWheel: ProgressWheel,
 videoprogresslay: RelativeLayout, videoprogressbar: ProgressWheel, dbhelper: DatabaseHandler,
 editorData: SharedPreferences.Editor, prefData: SharedPreferences,
 userId: String, thumbFile: File, uploadimage: ImageView) {
    private val TAG = "ImageDownloader"
    private val rotatedBitmap: Bitmap? = null
    var orientation = 0

    private fun getFileName(url: String): String {
        var imgSplit = url
        val endIndex = imgSplit.lastIndexOf("/")
        if (endIndex != -1) {
            imgSplit = imgSplit.substring(endIndex + 1, imgSplit.length)
        }
        return imgSplit
    }

    //
    //    protected abstract void onPostExecute(Bitmap imgBitmap);
    //
    //    protected abstract void onProgressUpdate(String... progress);
    private fun downloadMedia(url: String, path: String, fileName: String, progressWheel: ProgressWheel,
                              videoprogresslay: RelativeLayout, videoprogressbar: ProgressWheel, dbhelper: DatabaseHandler,
                              editorData: SharedPreferences.Editor, prefData: SharedPreferences,
                              userId: String, thumbFile: File, uploadimage: ImageView): Int {

        videoprogresslay.visibility = View.VISIBLE
        videoprogressbar.visibility = View.VISIBLE
        videoprogressbar.spin()
        return PRDownloader.download(url, path, fileName)
                .build()
                .setOnStartOrResumeListener { }
                .setOnPauseListener { }
                .setOnCancelListener { }
                .setOnProgressListener { progress -> progressWheel.progress = progress.currentBytes / progress.totalBytes * 100.toFloat() }
                .start(object : OnDownloadListener {
                    override fun onDownloadComplete() {
                        videoprogresslay.visibility = View.GONE
                        videoprogressbar.visibility = View.GONE
                        val downPath = ""
                        val size: Long = downPath.length.toLong()

                        if (prefData.getLong("mediaDownload", 0) == 0L) {
                            editorData.putLong("mediaDownload", size)
                        } else {
                            editorData.putLong("mediaDownload", prefData.getLong("mediaDownload", 0) + size)
                        }
                        editorData.apply()
                        editorData.commit()

                        val dataStorageModel: DataStorageModel = dbhelper.getRecord(userId)

                        dbhelper.addDataStorage(userId,
                                dataStorageModel.message_count,
                                dataStorageModel.sent_contact,
                                dataStorageModel.sent_location,
                                dataStorageModel.sent_photos, (dataStorageModel.sent_videos.toLong() + 1).toString(),
                                dataStorageModel.sent_aud,
                                dataStorageModel.sent_doc,
                                dataStorageModel.sent_photos_size, (dataStorageModel.sent_videos_size.toLong() + size).toString(),
                                dataStorageModel.sent_aud_size,
                                dataStorageModel.sent_doc_size)

                        videoprogressbar.stopSpinning()
                        if (downPath == null) {
                            Log.v("Download Failed", "Download Failed")
                            Toast.makeText(context, "5:" + context.getString(R.string.something_wrong), Toast.LENGTH_SHORT).show()
                        } else {
                            Glide.with(context).load(Uri.fromFile(thumbFile)).thumbnail(0.5f)
                                    .into(uploadimage)
                            //  Toast.makeText(mContext, getString(R.string.downloaded), Toast.LENGTH_SHORT).show();
                        }

                    }

                    override fun onError(error: Error) {
                        Toast.makeText(context, "6:" + context.getString(R.string.something_wrong), Toast.LENGTH_SHORT).show()
                        videoprogresslay.visibility = View.GONE
                        videoprogressbar.visibility = View.GONE
                        videoprogressbar.stopSpinning()
                    }
                })
    }

    init {
        val imageStorage = StorageManager.getInstance(context)
        val filename = getFileName(url)
        downloadMedia(url, imageStorage.getPath(from), filename, progressWheel, videoprogresslay, videoprogressbar, dbhelper,
                editorData, prefData,
                userId, thumbFile, uploadimage)
    }
}