package com.topzi.chat.viewholder.status

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.status_header_item.view.*

class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var tv_header=view.tv_header
}