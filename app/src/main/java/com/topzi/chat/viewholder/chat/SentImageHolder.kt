package com.topzi.chat.viewholder.chat

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.chat_image_bubble_sent.view.*
import kotlinx.android.synthetic.main.lyt_reply.view.*

class SentImageHolder(view: View) : RecyclerView.ViewHolder(view) {
    var uploadimage=view.uploadimage
    var timeText=view.text_message_time
    var tickimage=view.tickimage
    var progresslay=view.progresslay
    var progressbar=view.progressbar
    var downloadicon=view.downloadicon
    var replyLayout=view.lyt_reply
    var txtReplyUserName=view.txt_user_name
    var txtReplyMsg=view.txt_message
    var imgReply=view.img_reply
    var ivStar=view.ivStar
}