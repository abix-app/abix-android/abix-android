package com.topzi.chat.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.chat_file_bubble_received.view.*
import kotlinx.android.synthetic.main.item_star_message_top.view.*

class StarFileViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var profileImage=view.profileImage
    var tvsenderName=view.tvsenderName
    var tv_receiverName=view.tv_receiverName
    var tv_time_top=view.tv_time
    var icon=view.icon
    var file_type_tv=view.file_type_tv
    var filename=view.filename
    var progressbar=view.progressbar
    var downloadicon=view.downloadicon
    var ivStar=view.ivStar
    var text_message_time=view.text_message_time
    var file_body_lay=view.file_body_lay
}