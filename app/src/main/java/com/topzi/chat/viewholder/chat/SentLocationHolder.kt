package com.topzi.chat.viewholder.chat

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.topzi.chat.R
import com.topzi.chat.activity.ApplicationClass
import com.topzi.chat.activity.LocationActivity
import com.topzi.chat.model.MessagesData
import kotlinx.android.synthetic.main.chat_location_bubble_sent.view.*
import kotlinx.android.synthetic.main.lyt_reply.view.*

class SentLocationHolder(view: View) : RecyclerView.ViewHolder(view), OnMapReadyCallback{
    var mapview=view.mapView
    var tickimage=view.tickimage
    var timeText=view.text_message_time
    var replyLayout=view.lyt_reply
    var txtReplyUserName=view.txt_user_name
    var txtReplyMsg=view.txt_message
    var imgReply=view.img_reply
    var ivStar=view.ivStar
    var message: MessagesData? = null
    var googleMap: GoogleMap? = null
    var mContext:Context?=null

    fun bind(mContext:Context,message: MessagesData) {
        this.mContext=mContext
        this.message = message
        if (mapview != null)
        {
            mapview.onCreate(null)
            mapview.onResume()
            mapview.getMapAsync(this)
        }
        if (message.isStared == "true") ivStar.visibility = View.VISIBLE else ivStar.visibility = View.GONE
        when (message.delivery_status) {
            "read" -> {
                tickimage.visibility = View.VISIBLE
                tickimage.setImageResource(R.drawable.double_tick)
                tickimage.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent), PorterDuff.Mode.MULTIPLY)
            }
            "sent" -> {
                tickimage.visibility = View.VISIBLE
                tickimage.setImageResource(R.drawable.double_tick_unseen)
                tickimage.setColorFilter(ContextCompat.getColor(mContext, R.color.white), PorterDuff.Mode.MULTIPLY)
            }
            else -> {
                tickimage.visibility = View.VISIBLE
                tickimage.setImageResource(R.drawable.single_tick)
                tickimage.setColorFilter(ContextCompat.getColor(mContext, R.color.white), PorterDuff.Mode.MULTIPLY)
            }
        }
        timeText.text = ApplicationClass.getTime(message.chat_time)
//        if (message.groupId != null && message.groupId != "")
//            showGroupReply(message.groupId, message.reply_to, replyLayout, txtReplyUserName, txtReplyMsg, imgReply)
//        else
//            showReply(message, replyLayout, txtReplyUserName, txtReplyMsg, imgReply)
//        if (selectedChatPos.contains(message)) {
//            itemView.isSelected = true
//        } else {
//            itemView.isSelected = false
//        }
    }

    override fun onMapReady(map: GoogleMap) {
        MapsInitializer.initialize(mContext)
        googleMap = map
        if (message != null && map != null) {
            googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(message!!.getLat().toDouble(), message!!.getLon().toDouble()), 12f))
            googleMap?.addMarker(MarkerOptions()
                    .position(LatLng(message!!.getLat().toDouble(), message!!.getLon().toDouble())))
            googleMap?.setOnMapClickListener {
                val i = Intent(mContext, LocationActivity::class.java)
                i.putExtra("from", "view")
                i.putExtra("lat", message?.lat)
                i.putExtra("lon", message?.lon)
                mContext?.startActivity(i)
            }
            //                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(message.getLat()), Double.parseDouble(message.getLon())), 15);
//                    map.animateCamera(cameraUpdate);
        }
    }
}