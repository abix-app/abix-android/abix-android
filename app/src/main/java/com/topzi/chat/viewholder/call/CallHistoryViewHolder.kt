package com.topzi.chat.viewholder.call

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_call.view.*
import kotlinx.android.synthetic.main.multiple_imageview.view.*

class CallHistoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var parentLay=view.parentLay
    var txtName=view.txtName
    var txtTime=view.txtTime
    var profileImage=view.profileImage
    var statusIcon=view.statusIcon
    var callType=view.callType
    var profileView=view.profileView
    var iv_left=view.iv_left
    var iv_top_right=view.iv_top_right
    var iv_bottom_right=view.iv_bottom_right
    var iv_parentlay=view.iv_parentlay
}