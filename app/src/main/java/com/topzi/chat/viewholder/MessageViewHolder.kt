package com.topzi.chat.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.chat_text_bubble_receive.view.*
import kotlinx.android.synthetic.main.item_star_message_top.view.*

class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var profileImage=view.profileImage
    var tvsenderName=view.tvsenderName
    var tv_receiverName=view.tv_receiverName
    var tv_time_top=view.tv_time
    var incoming_layout_bubble=view.incoming_layout_bubble
    var ivStar=view.ivStar
    var text_message_time=view.text_message_time
    var text_message_body=view.text_message_body
}