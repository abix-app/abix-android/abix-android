package com.topzi.chat.viewholder.chat

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.chat_image_bubble_receive.view.*
import kotlinx.android.synthetic.main.lyt_reply.view.*

class ReceivedImageHolder(view: View) : RecyclerView.ViewHolder(view)  {
    var uploadimage=view.uploadimage
    var progresslay=view.progresslay
    var timeText=view.text_message_time
    var progressbar=view.progressbar
    var downloadicon=view.downloadicon
    var videoprogresslay=view.videoprogresslay
    var videoprogressbar=view.videoprogressbar
    var replyLayout=view.lyt_reply
    var txtReplyUserName=view.txt_user_name
    var txtReplyMsg=view.txt_message
    var imgReply=view.img_reply
    var ivStar=view.ivStar


}