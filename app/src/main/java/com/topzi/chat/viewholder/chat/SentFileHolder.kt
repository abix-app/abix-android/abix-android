package com.topzi.chat.viewholder.chat

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.chat_file_bubble_sent.view.*

import kotlinx.android.synthetic.main.lyt_reply.view.*

class SentFileHolder(view: View) : RecyclerView.ViewHolder(view) {
    var filename=view.filename
    var timeText=view.text_message_time
    var tickimage=view.tickimage
    var icon=view.icon
    var file_body_lay=view.file_body_lay
    var progressbar=view.progressbar
    var uploadicon=view.uploadicon
    var file_type_tv=view.file_type_tv
    var replyLayout=view.lyt_reply
    var txtReplyUserName=view.txt_user_name
    var txtReplyMsg=view.txt_message
    var imgReply=view.img_reply
    var ivStar=view.ivStar
}