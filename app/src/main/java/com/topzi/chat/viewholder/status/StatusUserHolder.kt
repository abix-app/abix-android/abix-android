package com.topzi.chat.viewholder.status

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.status_user_item.view.*

class StatusUserHolder(view: View) : RecyclerView.ViewHolder(view) {
    var profileimage=view.profileimage
    var circular_status_view=view.circular_status_view
    var tv_name=view.tv_name
    var tv_time=view.tv_time
    var profileview=view.profileview
    var menu_details=view.menu_details;
}