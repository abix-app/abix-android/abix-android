package com.topzi.chat.viewholder.chat

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.chat_text_bubble_receive.view.*
import kotlinx.android.synthetic.main.lyt_reply.view.*

class ReceivedMessageHolder(view: View) : RecyclerView.ViewHolder(view) {
    var messageText=view.text_message_body
    var timeText=view.text_message_time
    var replyLayout=view.lyt_reply
    var txtReplyUserName=view.txt_user_name
    var txtReplyMsg=view.txt_message
    var imgReply=view.img_reply
    var ivStar=view.ivStar
}