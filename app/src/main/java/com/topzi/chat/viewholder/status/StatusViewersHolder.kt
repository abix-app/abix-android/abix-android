package com.topzi.chat.viewholder.status

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_status_viewers.view.*

class StatusViewersHolder(view: View) : RecyclerView.ViewHolder(view) {
    var profileimage=view.profileimage
    var name=view.name
    var time=view.time
}