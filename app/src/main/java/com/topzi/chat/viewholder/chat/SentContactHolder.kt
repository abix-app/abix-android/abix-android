package com.topzi.chat.viewholder.chat

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.chat_contact_bubble_sent.view.*
import kotlinx.android.synthetic.main.lyt_reply.view.*

class SentContactHolder(view: View) : RecyclerView.ViewHolder(view) {
    var username=view.username
    var phoneno=view.phoneno
    var tickimage=view.tickimage
    var timeText=view.text_message_time
    var replyLayout=view.lyt_reply
    var txtReplyUserName=view.txt_user_name
    var txtReplyMsg=view.txt_message
    var imgReply=view.img_reply
    var ivStar=view.ivStar
}