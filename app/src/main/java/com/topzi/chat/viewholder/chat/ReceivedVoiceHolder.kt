package com.topzi.chat.viewholder.chat

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.chat_voice_bubble_received.view.*
import kotlinx.android.synthetic.main.lyt_reply.view.*

class ReceivedVoiceHolder(view: View) : RecyclerView.ViewHolder(view) {
    var filename=view.filename
    var timeText=view.text_message_time
    var playIcon=view.playIcon
    var file_body_lay=view.file_body_lay
    var downloadicon=view.downloadicon
    var progressbar=view.progressbar
    var file_type_tv=view.file_type_tv
    var seekBar=view.seekBar
    var replyLayout=view.lyt_reply
    var txtReplyUserName=view.txt_user_name
    var txtReplyMsg=view.txt_message
    var imgReply=view.img_reply
    var ivStar=view.ivStar
}