package com.topzi.chat.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.topzi.chat.R;
import com.topzi.chat.helper.DatabaseHandler;
import com.topzi.chat.model.UserProfileModel;
import com.topzi.chat.utils.ApiClient;
import com.topzi.chat.utils.ApiInterface;
import com.topzi.chat.utils.Constants;
import com.topzi.chat.utils.GetSet;
import com.topzi.chat.helper.OkCallback;
import com.topzi.chat.utils.ObjectSerializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.topzi.chat.utils.Constants.TAG_EVERYONE;
import static com.topzi.chat.utils.Constants.TAG_MY_CONTACTS;
import static com.topzi.chat.utils.Constants.TAG_NOBODY;

public class PrivacyActivity extends BaseActivity implements View.OnClickListener {

    private final String TAG = this.getClass().getSimpleName();
    static ApiInterface apiInterface;
    ProgressDialog progressDialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    DatabaseHandler dbhelper;
    Toolbar toolbar;
    ImageView btnBack;
    TextView txtTitle;
    TextView txtLastSeen, txtProfilePhoto, txtAbout, txtStatus, txtBlocked, txtReceipt;
    LinearLayout lastSeenLay, profilePhotoLay, aboutLay, statusLay, blockedLay, receiptLay,unHideLay;
    RelativeLayout messageLay,rlReadRecie;
    PrivacyDialogFragment privacyDialogFragment;
    Switch readSwitch;


    /**
     * Edit by Sujith
     * */
    private ArrayList<String> hideChatID = new ArrayList<>();

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        pref = PrivacyActivity.this.getSharedPreferences("SavedPref", MODE_PRIVATE);
        editor = pref.edit();
        dbhelper = DatabaseHandler.getInstance(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.pleasewait));
        progressDialog.setCancelable(false);

        toolbar = findViewById(R.id.actionbar);
        btnBack = toolbar.findViewById(R.id.backbtn);
        txtTitle = toolbar.findViewById(R.id.title);
        unHideLay = findViewById(R.id.unHideLay);
        txtLastSeen = findViewById(R.id.txtLastSeen);
        txtProfilePhoto = findViewById(R.id.txtProfilePhoto);
        txtAbout = findViewById(R.id.txtAbout);
        txtStatus = findViewById(R.id.txtStatus);
        txtBlocked = findViewById(R.id.txtBlocked);
        txtReceipt = findViewById(R.id.txtReceipt);
        lastSeenLay = findViewById(R.id.lastSeenLay);
        profilePhotoLay = findViewById(R.id.photoLay);
        aboutLay = findViewById(R.id.aboutLay);
        messageLay = findViewById(R.id.messageLay);
        statusLay = findViewById(R.id.statusLay);
        blockedLay = findViewById(R.id.blockedLay);
        receiptLay = findViewById(R.id.receiptLay);
        rlReadRecie = findViewById(R.id.rlReadRecie);
        readSwitch = findViewById(R.id.readSwitch);

        initToolBar();
        initData();

        if (pref.getBoolean("readReciept",true))
            readSwitch.setChecked(true);
        else
            readSwitch.setChecked(false);

        readSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (pref.getBoolean("readReciept",true)) {
                readSwitch.setChecked(false);
                editor.putBoolean("readReciept", false);
            } else {
                readSwitch.setChecked(true);
                editor.putBoolean("readReciept", true);
            }
            editor.apply();
            editor.commit();
        });

        lastSeenLay.setOnClickListener(this);
        profilePhotoLay.setOnClickListener(this);
        aboutLay.setOnClickListener(this);
        messageLay.setOnClickListener(this);
        unHideLay.setOnClickListener(this);
        statusLay.setOnClickListener(this);
        blockedLay.setOnClickListener(this);
        receiptLay.setOnClickListener(this);
        rlReadRecie.setOnClickListener(this);
    }

    @Override
    public void onNetworkChange(boolean isConnected) {

    }

    private void initToolBar() {
        txtTitle.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.VISIBLE);
        txtTitle.setText(getString(R.string.privacy));
        setSupportActionBar(toolbar);
//        btnBack.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.primarytext));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initData() {
        String lastSeen = GetSet.getPrivacylastseen();
        String profileImage = GetSet.getPrivacyprofileimage();
        String about = GetSet.getPrivacyabout();

        if (lastSeen != null) {
            if (lastSeen.equalsIgnoreCase(TAG_EVERYONE)) {
                txtLastSeen.setText(getString(R.string.everyone));
            } else if (lastSeen.equalsIgnoreCase(TAG_NOBODY)) {
                txtLastSeen.setText(getString(R.string.nobody));
            } else {
                txtLastSeen.setText(getString(R.string.my_contacts));
            }
        }

        if (profileImage != null) {
            if (profileImage.equalsIgnoreCase(TAG_EVERYONE)) {
                txtProfilePhoto.setText(getString(R.string.everyone));
            } else if (profileImage.equalsIgnoreCase(TAG_NOBODY)) {
                txtProfilePhoto.setText(getString(R.string.nobody));
            } else {
                txtProfilePhoto.setText(getString(R.string.my_contacts));
            }
        }

        if (about != null) {
            if (about.equalsIgnoreCase(TAG_EVERYONE)) {
                txtAbout.setText(getString(R.string.everyone));
            } else if (about.equalsIgnoreCase(TAG_NOBODY)) {
                txtAbout.setText(getString(R.string.nobody));
            } else {
                txtAbout.setText(getString(R.string.my_contacts));
            }
        }
    }

    @Override
    protected void onResume() {
        if (dbhelper != null) {
            int blockedSize = dbhelper.getBlockedContacts(this).size();
            if (blockedSize > 0) {
                txtBlocked.setText(getString(R.string.blocked_contacts) + " " + blockedSize);
            } else {
                txtBlocked.setText(getString(R.string.blocked_contacts));
            }
        }
        if (pref.getBoolean("readReciept",true))
            readSwitch.setChecked(true);
        else
            readSwitch.setChecked(false);

        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lastSeenLay:
                openPrivacyDialog(R.id.lastSeenLay, txtLastSeen.getText(), getString(R.string.last_seen));
                break;
            case R.id.photoLay:
                openPrivacyDialog(R.id.photoLay, txtProfilePhoto.getText(), getString(R.string.profile_photo));
                break;
            case R.id.aboutLay:
                openPrivacyDialog(R.id.aboutLay, txtAbout.getText(), getString(R.string.about));
                break;
            case R.id.statusLay:
//                openPrivacyDialog(R.id.statusLay, txtStatus.getText(), getString(R.string.status));
                break;
            case R.id.rlReadRecie:
                if (pref.getBoolean("readReciept",true)) {
                    readSwitch.setChecked(false);
                    editor.putBoolean("readReciept", false);
                } else {
                    readSwitch.setChecked(true);
                    editor.putBoolean("readReciept", true);
                }
                editor.apply();
                editor.commit();
                break;
            case R.id.messageLay:

                break;
            case R.id.blockedLay:
                Intent block = new Intent(getApplicationContext(), BlockedContactsActivity.class);
                startActivity(block);
                break;
            case R.id.unHideLay:
                checkHideChatsPin();
                break;
            case R.id.receiptLay:

                break;
        }
    }

    private void openPrivacyDialog(final int id, CharSequence text, String title) {
        privacyDialogFragment = new PrivacyDialogFragment();
//        privacyDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        privacyDialogFragment.setCancelable(true);
        privacyDialogFragment.setSelected(text);
        privacyDialogFragment.setTitle(title);
        privacyDialogFragment.setCallBack(new OkCallback() {
            @Override
            public void onOkClicked(Object object) {
                privacyDialogFragment.dismiss();
                switch (id) {
                    case R.id.lastSeenLay:
                        txtLastSeen.setText((String) object);
                        break;
                    case R.id.photoLay:
                        txtProfilePhoto.setText((String) object);
                        break;
                    case R.id.aboutLay:
                        txtAbout.setText((String) object);
                        break;
                    case R.id.statusLay:
                        txtStatus.setText((String) object);
                        break;
                    case R.id.messageLay:

                        break;
                    case R.id.blockedLay:

                        break;
                    case R.id.receiptLay:

                        break;
                }

                updatePrivacy();
            }
        });
        privacyDialogFragment.show(getSupportFragmentManager(), TAG);
    }

    private void updatePrivacy() {

        final String lastSeen = checkString(txtLastSeen.getText().toString().trim().toLowerCase());
        final String profileImage = checkString(txtProfilePhoto.getText().toString().trim().toLowerCase());
        final String about = checkString(txtAbout.getText().toString().trim().toLowerCase());

        editor.putString("privacyprofileimage", profileImage);
        editor.putString("privacylastseen", lastSeen);
        editor.putString("privacyabout", about);
        editor.commit();

        GetSet.setPrivacyprofileimage(pref.getString("privacyprofileimage", Constants.TAG_EVERYONE));
        GetSet.setPrivacylastseen(pref.getString("privacylastseen", Constants.TAG_EVERYONE));
        GetSet.setPrivacyabout(pref.getString("privacyabout", Constants.TAG_EVERYONE));

        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.TAG_USER_ID, GetSet.getUserId());
        map.put(Constants.TAG_PRIVACY_LAST_SEEN, lastSeen);
        map.put(Constants.TAG_PRIVACY_PROFILE, profileImage);
        map.put(Constants.TAG_PRIVACY_ABOUT, about);

        Call<UserProfileModel> call3 = apiInterface.updateMyPrivacy(GetSet.getUserId(), lastSeen, profileImage, about);
        call3.enqueue(new Callback<UserProfileModel>() {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                UserProfileModel userdata = response.body();
//                if (userdata.get("status").equals("true")) {
//
//                }
            }

            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {
                call.cancel();
//                Log.e(TAG, "updateMyPrivacy: " + t.getMessage());
            }
        });
    }

    private String checkString(String privacy) {
        if (privacy.equalsIgnoreCase(getString(R.string.everyone))) {
            privacy = TAG_EVERYONE;
        } else if (privacy.equalsIgnoreCase(getString(R.string.nobody))) {
            privacy = TAG_NOBODY;
        } else {
            privacy = TAG_MY_CONTACTS;
        }
        return privacy;
    }

    /**
     * Edit by Sujith
     * Fuction to open validate pin dialog.
     * */
    private void checkHideChatsPin() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialoge_set_pin);
        dialog.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels * 90 / 100, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        EditText etPin = dialog.findViewById(R.id.etPin);
        TextView title = dialog.findViewById(R.id.title);
        TextView yes = dialog.findViewById(R.id.yes);
        TextView no = dialog.findViewById(R.id.no);

        title.setText("Enter the pin");

        no.setVisibility(View.VISIBLE);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etPin.getText().toString().trim().equals(pref.getString("hideChatPin", ""))) {
                    etPin.setError("Invalid Pin");
                } else {
                    dialog.dismiss();
                    Constants.isShow = true;
                    setUnHideChat();
//                    Intent intent = new Intent(MainActivity.this, HideChatActivity.class);
//                    startActivity(intent);
//                    finish();
                }
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    /**
     * Edit by Sujith
     * Fuction to get the hidden chat Ids.
     * */
    private ArrayList<String> getHideChat() {
        if (null == hideChatID) {
            hideChatID = new ArrayList<>();
        }

        try {
            hideChatID = (ArrayList<String>) ObjectSerializer.deserialize(pref.getString("hideId", ObjectSerializer.serialize(new ArrayList<String>())));
            Log.e("LLLLL_Hide: ", String.valueOf(hideChatID));

            return hideChatID;
        } catch (IOException e) {
            Log.e("LLLLLLL_EX11: ", Objects.requireNonNull(e.getMessage()));
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
    /**
     * Edit by Sujith
     * Fuction to clear chat Ids for unhiding.
     * */
    private void setUnHideChat() {
        if (!getHideChat().isEmpty()) {

            hideChatID.clear();

            Log.e("LLLLL_Hide11: ", String.valueOf(hideChatID));
            try {
                editor.putString("hideId", ObjectSerializer.serialize(hideChatID));
            } catch (IOException e) {
                Log.e("LLLLLLL_HideE11: ", Objects.requireNonNull(e.getMessage()));
                e.printStackTrace();
            }
            editor.commit();
            Toast.makeText(this, "Successfully unhided the chats...", Toast.LENGTH_SHORT).show();


        } else {
            Toast.makeText(this, "No any hidden chat there...", Toast.LENGTH_SHORT).show();
        }
    }

}
