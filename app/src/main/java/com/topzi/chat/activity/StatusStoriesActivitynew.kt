package com.topzi.chat.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bolaware.viewstimerstory.Momentz
import com.bolaware.viewstimerstory.MomentzCallback
import com.bolaware.viewstimerstory.MomentzView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.topzi.chat.BuildConfig
import com.topzi.chat.R
import com.topzi.chat.adapters.StatusViewersAdapter
import com.topzi.chat.helper.*
import com.topzi.chat.model.MessagesData
import com.topzi.chat.model.status.BaseStatusDto
import com.topzi.chat.utils.Constants
import com.topzi.chat.utils.GetSet
import com.topzi.chat.utils.MyUtils
import com.topzi.chat.utils.ToastUtils
import kotlinx.android.synthetic.main.activity_status_stories.*
import kotlinx.android.synthetic.main.audio_vishualizer_view.*
import kotlinx.android.synthetic.main.bottom_sheet_viewer_list.*
import kotlinx.android.synthetic.main.status_actionbar.view.*
import org.json.JSONException
import org.json.JSONObject
import toPixel
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class StatusStoriesActivitynew: AppCompatActivity(), MomentzCallback{
    private val TAG ="StatusStoriesActivity"

    var dbhelper: DatabaseHandler? = null
    var allStatus: ArrayList<MessagesData>? = null
    var storageManager: StorageManager? = null
    var prefData: SharedPreferences? = null
    var position: Int? = 0
    var editorData: SharedPreferences.Editor? = null
    var mediaPlayer: MediaPlayer?= null
    var socketConnection: SocketConnection?= null
    var dataStatus :HashMap<String,String>?=null
    var mainList: ArrayList<BaseStatusDto>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status_stories)
        init()
        initViews()
    }
    fun init(){
        //        headerLayout.setVisibility(View.VISIBLE);
        socketConnection = SocketConnection.getInstance(this)

        prefData = getSharedPreferences(Constants.NETWORK_USAGE, Context.MODE_PRIVATE)
        editorData = prefData?.edit()
        storageManager = StorageManager.getInstance(this)
        dbhelper = DatabaseHandler.getInstance(this)
        val sender_id=intent.getStringExtra("sender_id")
         position=intent.getIntExtra("position",0)
        if(!sender_id.equals(GetSet.getUserId()))
        {
            bottom_sheet.visibility = GONE;
        }
        intent.hasExtra("status")
        dataStatus=intent.getSerializableExtra("status") as HashMap<String, String>
        mainList=intent.getSerializableExtra("mainList") as ArrayList<BaseStatusDto>
        allStatus = this.dbhelper?.getAllStatus(sender_id, "0") as ArrayList<MessagesData>?
        bt_delete.setOnClickListener {
            emitStatusDelete(allStatus!!.get(currentIndex).messageId)
            dbhelper!!.deleteStatus(allStatus!!.get(currentIndex).messageId)
            allStatus!!.removeAt(currentIndex)
            currentMomentz!!.delete()
        }
//        mVisualizer = findViewById(R.id.blob)

//        mVisualizer?.setDrawLine(true)
    }
    fun initNext(sender_id:String,dataStatusnew :HashMap<String,String>){
        //        headerLayout.setVisibility(View.VISIBLE);


        bottom_sheet.visibility = GONE;
        dataStatus = dataStatusnew
        allStatus = this.dbhelper?.getAllStatus(sender_id, "0") as ArrayList<MessagesData>?
        bt_delete.setOnClickListener {
            emitStatusDelete(allStatus!!.get(currentIndex).messageId)
            dbhelper!!.deleteStatus(allStatus!!.get(currentIndex).messageId)
            allStatus!!.removeAt(currentIndex)
            currentMomentz!!.delete()
        }
        initViews()
//        mVisualizer = findViewById(R.id.blob)

//        mVisualizer?.setDrawLine(true)
    }

    var viewMap = HashMap<Int,View>()
    fun setTopbarView(index: Int){
        if(index >0) {
            if(viewMap.containsKey(index-1))
            container.removeView(viewMap.get(index-1));
        }
        val customView = LayoutInflater.from(this).inflate(R.layout.status_actionbar, null)
        container.addView(customView)
        viewMap.put(index,customView);
        customView.iv_back.setOnClickListener { finish() }
        if(GetSet.getUserId().equals(dataStatus?.get(Constants.TAG_USER_ID)))
        {
            customView.userName.text= "My Status"

        }
        else{
            customView.userName.text= dataStatus?.get(Constants.TAG_USER_NAME)

        }
        if(!this.isDestroyed)
            Glide.with(this).load(Constants.USER_IMG_PATH +dbhelper?.getContactDetail(dataStatus?.get(Constants.TAG_USER_ID)?.toInt())?.user_image).thumbnail(0.5f)
                    .apply(RequestOptions.circleCropTransform()
                            .placeholder(R.drawable.person)
                            .error(R.drawable.person)
                            .override(ApplicationClass.dpToPx(this, 70)))
                    .into(customView.userImg)
    //    customView.tv_time.setText(ApplicationClass.getTime(allStatus?.get(index)?.chat_time!!))
      customView.tv_time.setText(getFormattedDate(this,allStatus?.get(index)?.chat_time!!))

    }
    fun initViews(){
        // show a textview
        val textView = TextView(this)
        textView.text = "Hello, You can display TextViews"
        textView.textSize = 20f.toPixel(this).toFloat()
        textView.gravity = Gravity.CENTER
        textView.setTextColor(Color.parseColor("#ffffff"))

        //show a customView
//        val customView = LayoutInflater.from(this).inflate(R.layout.custom_view, null)

        //show a custome audio player
        var customaudioView :ViewGroup

        // show an imageview be loaded from file
        var locallyLoadedImageView = ImageView(this)
        locallyLoadedImageView.setImageDrawable(
                ContextCompat.getDrawable(
                        this,
                        R.drawable.image_placeholder
                )
        )

        //image to be loaded from the internet
        var internetLoadedImageView = ImageView(this)

        //video to be loaded from the internet
        var videoPlayer :VideoView
//        val internetLoadedVideo = BetterVideoPlayer(this)

        val viewlist:ArrayList<MomentzView>?= ArrayList()
        for (status in allStatus!!) {
            when(status.message_type){
                "text"->viewlist?.add(MomentzView(textView, 5))
                "image"->if (!storageManager?.checkifImageExists("thumb", status.attachment)!!||!storageManager?.checkifImageExists(status?.attachment, "sent")!!) {
                    internetLoadedImageView = ImageView(this)
                    viewlist?.add(MomentzView(internetLoadedImageView, 6))
                } else{
                    locallyLoadedImageView = ImageView(this)
                    var yourDrawable: Drawable?=null
                    try {
                        var file:File?=null;
                        if (storageManager?.checkifImageExists("thumb", status.attachment)!!)
                            file = storageManager?.getImage("thumb", status.attachment)!!
                        else if(storageManager?.checkifImageExists("sent",status?.attachment )!!)
                            file = storageManager?.getImage("sent", status.attachment)!!
                        if(file!=null){
                            val bitmap: Bitmap = BitmapFactory.decodeFile(file?.absolutePath)
                            val drawable: Drawable = BitmapDrawable(resources, bitmap)
                            yourDrawable=drawable
                        }

                    } catch (e: java.lang.Exception) {
                        yourDrawable=ContextCompat.getDrawable(this, R.drawable.image_placeholder)
                    }
                    if(yourDrawable!=null){
                        locallyLoadedImageView.setImageDrawable(yourDrawable)
                        viewlist?.add(MomentzView(locallyLoadedImageView, 6))
                    }
                }
                "video"-> {
                    videoPlayer = VideoView(this)
                    viewlist?.add(MomentzView(videoPlayer, 60))
                }
                "audio"->{
                    customaudioView = LayoutInflater.from(this).inflate(R.layout.audio_vishualizer_view, null) as ViewGroup
                    viewlist?.add(MomentzView(customaudioView, 60))
                }
            }
        }
        if(viewlist?.size!! >0)
            Momentz(this, viewlist, container, this).start()
    }


    override fun done() {
        finish()
    }
   var currentMomentz: Momentz? = null;
    var currentIndex: Int = 0;
    override fun onNextCalled(view: View, momentz: Momentz, index: Int) {
        currentMomentz = momentz
        currentIndex = index
        var useidSender = allStatus?.get(index)!!.user_id.toString();
        var useridRec = GetSet.getUserId();
        if(useidSender.equals(useridRec)) {
            var statusViewers = dbhelper!!.getAllStatusViewers(allStatus?.get(index)!!.messageId)
            var statusViewersAdapter = StatusViewersAdapter(this, statusViewers)
            rv_viewerslist.adapter = statusViewersAdapter;
            var lineraLayoutMGR = LinearLayoutManager(this)
            rv_viewerslist.layoutManager = lineraLayoutMGR
            tv_title.text = "Viewed By "+statusViewers.size
            tv_count.text = ""+statusViewers.size
        }
        else{
            emitStatusView(allStatus?.get(index)!!)
            dbhelper!!.updateStatusData(allStatus?.get(index)!!.messageId,Constants.TAG_STATUS_VIEWED,"yes")
        }

        if(this.mediaPlayer!=null)
            clearMediaPlayer(this.mediaPlayer!!)
        if (view is VideoView) {
            momentz.pause(true)
            initVideo(view, index, momentz)
        } else if ((view is ImageView)&& (view.drawable == null)) {
            momentz.pause(true)
            showStatusImage(view,allStatus?.get(index)!!,momentz)
        }else if((view is ImageView)&&(view.drawable != null)){
            showStatusImage(view,allStatus?.get(index)!!,momentz)
        } else if(view is TextView){
            view.text=allStatus?.get(index)?.message
        }else{
            initAudio(view,momentz,index)
        }
        setTopbarView(index)
    }
    var exist = "yes"

    override fun onEndCalled() : String?{
        if(position!!+1 < mainList!!.size ) {
            if(mainList!!.get(position!!+1).type.equals("user")) {
                val hashMap: HashMap<String, String> = mainList!!.get(position!! + 1).status
                var allStatus1 = this.dbhelper?.getAllStatus(hashMap!!.get(Constants.TAG_USER_ID).toString(), "0") as ArrayList<MessagesData>?
                var isviewed = "yes";
                for (status in allStatus1!!)
                {
                    if(status.isViewed.equals("no"))
                    {
                        isviewed = "no"
                        break;
                    }

                }
                if(isviewed.equals("no")) {
                    initNext(hashMap!!.get(Constants.TAG_USER_ID).toString(), hashMap)


                    position = position!! + 1
                    if(position!! < mainList!!.size ) {
                        exist = "exist"
                    }
                    else{
                        exist =  "no"
                    }
                }
                else{
                    position = position!! + 1
                    onEndCalled()


                }
                return  exist;
            }
            else{
                position =     position!!+1
                onEndCalled()
            }

        }
        else{
            exist =  "no"

        }
        return exist;
    }

    fun initAudio(view: View, momentz: Momentz, index: Int){

        var mediaPlayer = MediaPlayer()
        this.mediaPlayer=mediaPlayer
        Log.e("filepath",allStatus?.get(index)?.attachment)
        if (storageManager?.checkifFileExists(allStatus?.get(index)?.attachment, allStatus?.get(index)?.message_type, "sent")!!){
            playVoice(mediaPlayer,allStatus?.get(index)!!)
        } else if (storageManager?.checkifFileExists(allStatus?.get(index)?.attachment, allStatus?.get(index)?.message_type, "receive")!!) {
            playVoice(mediaPlayer,allStatus?.get(index)!!)
        } else {
            if (MyUtils.isNetworkConnected(this) == NetworkUtil.NOT_CONNECT) {
                Toast.makeText(this,"No network connection!",Toast.LENGTH_SHORT).show()
            } else {
                try {
                    val downloadFiles: DownloadFiles = object : DownloadFiles(applicationContext) {
                        override fun onPostExecute(downPath: String) {
                            val file = File(downPath)
                            val size = file.length()
                            if (prefData?.getLong("statusDownload", 0) == 0L) {
                                editorData?.putLong("statusDownload", size)
                            } else {
                                editorData?.putLong("statusDownload", prefData?.getLong("statusDownload", 0)!! + size)
                            }
                            editorData?.apply()
                            val count = prefData?.getLong("statusDownMesCount", 0)
                            editorData?.putLong("statusDownMesCount", count !!+ 1)
                            editorData?.apply()
                            editorData?.commit()
//                        progressBar.setVisibility(View.GONE)
                            if (downPath == null) {
                                Log.e("LLLLL_bitmapFailed7: ", "Fild..")
                                Log.v("Download Failed", "Download Failed")
                                Toast.makeText(applicationContext, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show()
                            } else {
//                            Toast.makeText(getActivity(), getString(R.string.downloaded), Toast.LENGTH_SHORT).show();
                                val file = storageManager?.getFile(allStatus?.get(index)?.attachment, allStatus?.get(index)?.message_type, "receive")
                                val audURI = FileProvider.getUriForFile(applicationContext,
                                        BuildConfig.APPLICATION_ID + ".provider", file!!)
                                playVoice(mediaPlayer,allStatus?.get(index)!!)
                            }
                        }
                    }
                    downloadFiles.execute(Constants.CHAT_IMG_PATH + allStatus?.get(index)?.attachment, allStatus?.get(index)?.message_type)
                }catch (e:Exception){
                    e.printStackTrace()
                }
//                progressBar.setVisibility(View.VISIBLE)
            }
        }
    }
    fun initVideo(videoView: VideoView, index: Int, momentz: Momentz) {
        Log.e("filepath",allStatus?.get(index)?.attachment)
        if(allStatus?.get(index)?.attachment?.contains("/storage/emulated/0/")!!){
            playVideo(allStatus?.get(index)?.attachment!!,videoView,index,momentz)
        } else{
            if(storageManager?.checkifFileExists(allStatus?.get(index)?.attachment, allStatus?.get(index)?.message_type, "sent")!! &&
                    storageManager?.checkifImageExists("sent", allStatus?.get(index)?.thumbnail)!!){
                val file = storageManager?.getFile(allStatus?.get(index)?.attachment, allStatus?.get(index)?.message_type, "sent")
                val vidURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider", file!!)
                playVideo(vidURI.toString(),videoView,index,momentz)
                Log.e("vidURI",vidURI.toString())
            } else if (storageManager?.checkifFileExists(allStatus?.get(index)?.attachment, allStatus?.get(index)?.message_type, "receive")!! &&
                    storageManager?.checkifImageExists("thumb", allStatus?.get(index)?.thumbnail)!!) {
                val file = storageManager?.getFile(allStatus?.get(index)?.attachment, allStatus?.get(index)?.message_type, "receive")
                val vidURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider", file!!)
                Log.e("vidURI",vidURI.toString())

//             if(allStatus?.get(index)?.attachment?.contains("/storage/emulated/0/")!!)
//                 playVideo(allStatus?.get(index)?.attachment!!,videoView,index,momentz)
//             else
                playVideo(vidURI.toString(),videoView,index,momentz)

            }else{
                if (MyUtils.isNetworkConnected(this) == NetworkUtil.NOT_CONNECT) {
                    Toast.makeText(this,"No network connection!",Toast.LENGTH_SHORT).show()
                } else {
                    val imageDownloader: ImageDownloader = object : ImageDownloader(this) {
                        override fun onPostExecute(imgBitmap: Bitmap) {
                            if (imgBitmap == null) {
                                Log.e("LLLLL_bitmapFailed3: ", "Fild..")
                                Toast.makeText(applicationContext, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show()
                                //                            videoprogresslay.setVisibility(View.GONE);
//                            videoprogressbar.setVisibility(View.GONE);
//                            videoprogressbar.stopSpinning();
                            } else {
                                Log.v("onBitmapLoaded", "onBitmapLoaded")
                                try {
                                    val status = storageManager?.saveThumbNail(imgBitmap, allStatus?.get(index)?.thumbnail)
                                    if (status == "success") {
                                        val thumbFile = storageManager?.getImage("thumb", allStatus?.get(index)?.thumbnail)
                                        if (thumbFile != null) {
                                            Log.v("file", "file=" + thumbFile.absolutePath)
                                            val size = thumbFile.length()
                                            if (prefData?.getLong("statusDownload", 0) == 0L) {
                                                editorData?.putLong("statusDownload", size)
                                            } else {
                                                editorData?.putLong("statusDownload", prefData?.getLong("statusDownload", 0)!! + size)
                                            }
                                            editorData?.apply()
                                            val count = prefData?.getLong("statusDownMesCount", 0)
                                            editorData?.putLong("statusDownMesCount", count !!+ 1)
                                            editorData?.apply()
                                            editorData?.commit()
                                            val downloadFiles: DownloadFiles = object : DownloadFiles(applicationContext) {
                                                override fun onPostExecute(downPath: String) {
//                                                 progressBar.setVisibility(View.GONE)
                                                    //                                                videoprogressbar.setVisibility(View.GONE);
//                                                videoprogressbar.stopSpinning();
                                                    if (downPath == null) {
                                                        Log.e("LLLLL_bitmapFailed5: ", "Fild..")
                                                        Log.v("Download Failed", "Download Failed")
                                                        Toast.makeText(applicationContext, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show()
                                                    } else {
//                                                     Glide.with(getActivity()).load(Uri.fromFile(thumbFile)).thumbnail(0.5f)
//                                                             .into(imageView)
                                                        Log.e("vidURI",downPath.toString())
                                                        val file = storageManager?.getFile(allStatus?.get(index)?.attachment, allStatus?.get(index)?.message_type, "receive")
                                                        val vidURI = FileProvider.getUriForFile(applicationContext,
                                                                BuildConfig.APPLICATION_ID + ".provider", file!!)
                                                        Log.e("vidURI",vidURI.toString())
                                                        playVideo(vidURI.toString(),videoView,index,momentz)
                                                    }
                                                }
                                            }
                                            downloadFiles.execute(Constants.CHAT_IMG_PATH + allStatus?.get(index)?.attachment, allStatus?.get(index)?.message_type)
                                        }
                                    } else {
                                        Log.e("LLLLL_bitmapFailed6: ", "Fild..")
                                        Toast.makeText(applicationContext, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show()
                                        //                                    videoprogresslay.setVisibility(View.GONE);
//                                    videoprogressbar.setVisibility(View.GONE);
//                                    videoprogressbar.stopSpinning();
                                    }
                                } catch (e: java.lang.NullPointerException) {
                                    e.printStackTrace()
                                }
                            }
//                         showNextStatus(true)
                        }

                        override fun onProgressUpdate(vararg progress: String) {
//                         progressBar.setProgress(progress[0].toInt())
                        }
                    }
                    imageDownloader.execute(Constants.CHAT_IMG_PATH + allStatus?.get(index)?.thumbnail, "thumb")
//                 progressBar.setVisibility(View.VISIBLE)
//                videoprogressbar.setVisibility(View.VISIBLE);
//                videoprogressbar.spin();
                }
            }
        }

    }

    fun playVideo( uri:String,videoView: VideoView, index: Int, momentz: Momentz){
        videoView.setVideoURI(Uri.parse(uri))
        videoView.requestFocus()
        videoView.start()

        videoView.setOnInfoListener(object : MediaPlayer.OnInfoListener {
            override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    // Here the video starts
                    momentz.editDurationAndResume(index, (videoView.duration) / 1000)
                    momentz.resume()
//                    Toast.makeText(applicationContext, "Video loaded from the internet", Toast.LENGTH_LONG).show()
                    return true
                }
                return false
            }
        })
    }
    fun showStatusImage(view: ImageView, message:MessagesData, momentz: Momentz ) {
        Log.e("image_url",  message.attachment)

        if(storageManager?.checkifImageExists("sent", message?.attachment)!!||storageManager?.checkifImageExists("thumb", message.attachment)!!){
            var file:File ?=null
            if(storageManager?.checkifImageExists("sent", message?.attachment)!!)
                file = storageManager?.getImage("sent", message.attachment)!!
            else
                file = storageManager?.getImage("thumb", message.attachment)!!
            file?.getParentFile()?.mkdirs()
            val size = file?.length()
            if (prefData?.getLong("statusDownload", 0) == 0L) {
                editorData?.putLong("statusDownload", size!!)
            } else {
                editorData?.putLong("statusDownload", prefData?.getLong("statusDownload", 0) !!+ size!!)
            }
            editorData?.apply()

            editorData?.commit()
            if(file!=null){
                Log.v(TAG, "file=" + file.absolutePath)
//                val bitmap: Bitmap = BitmapFactory.decodeFile(file.absolutePath)
                if(!  this.isDestroyed)
                    Glide.with(this)
                            .load(file)
                            .thumbnail(0.5f)
                            .into(view)
            }
        } else{
            if (MyUtils.isNetworkConnected(this) == NetworkUtil.NOT_CONNECT) {
                Toast.makeText(this,"No network connection!",Toast.LENGTH_SHORT).show()
            } else {
                try{
                    val imageDownloader: ImageDownloader = (object : ImageDownloader(this) {
                        override fun onPostExecute(imgBitmap: Bitmap) {
                            if (imgBitmap == null) {
                                Log.e("LLLLL_bitmapFailed", "bitmapFailed")
                                ToastUtils.shortToast(getString(R.string.something_wrong))
                            } else {
                                Log.v("onBitmapLoaded", "onBitmapLoaded")
                                try {
//                                                            String[] fileName = message.attachment.split("/");
                                    val status = storageManager?.saveThumbNail(imgBitmap, message.attachment)
                                    if (status == "success") {
//                                        File thumbFile = storageManager.getImage("thumb", message.attachment);
//                                        Glide.with(getActivity()).load(thumbFile).thumbnail(0.5f)
//                                                .into(uploadimage);
//                                        progresslay.setVisibility(View.GONE);
//                                        progressbar.stopSpinning();
//                                        videoprogresslay.setVisibility(View.GONE);
                                        val file = storageManager?.getImage("thumb", message.attachment)
                                        val count = prefData?.getLong("statusDownMesCount", 0)
                                        editorData?.putLong("statusDownMesCount", count!! + 1)
                                        editorData?.apply()
                                        if (file != null) {
                                            Log.v(TAG, "file=" + file.absolutePath)
                                            Glide.with(applicationContext).load(file).thumbnail(0.5f)
                                                    .transition(DrawableTransitionOptions().crossFade())
                                                    .into(view)
                                        }
                                        momentz.resume()
//                                               }
                                    } else {
                                        Log.e("LLLLL_bitmapFailed1: ", "Fild..")
                                        ToastUtils.shortToast(getString(R.string.something_wrong))
                                    }
                                } catch (e: NullPointerException) {
                                    e.printStackTrace()
                                }
                            }
                            dbhelper?.updateStatusData(message.message_id, Constants.TAG_DELIVERY_STATUS, Constants.read)
                            // dbhelper.resetUnseenStatusCount(message.receiver_id);
                        }

                        override fun onProgressUpdate(vararg progress: String) {
//                        progressBar.setProgress(progress[0].toInt())
                        }
                    }).apply {
                        execute(Constants.CHAT_IMG_PATH +message.attachment, "receive")
                    }
                }catch (e:Exception){
                    e.printStackTrace()
                }

//                    progressbar.spin();
            }
        }

    }

    fun playVoice(mediaPlayer: MediaPlayer,data: MessagesData) {
        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                clearMediaPlayer(mediaPlayer)
                data.playProgress = 0
                data.isPlaying = true

            }
            if (!data.isPlaying) {
                mediaPlayer.setOnCompletionListener(MediaPlayer.OnCompletionListener {
                    Log.d("MediaPlayer", "Completed")
                    clearMediaPlayer(mediaPlayer)

                })

                //AssetFileDescriptor descriptor = getAssets().openFd("suits.mp3");
                var  file:File?=null
                if (storageManager?.checkifFileExists(data.attachment, data.message_type, "sent")!!){
                    file = storageManager?.getFile(data.attachment, data.message_type, "sent")
                }else if(storageManager?.checkifFileExists(data.attachment, data.message_type, "receive")!!){
                    file = storageManager?.getFile(data.attachment, data.message_type, "receive")
                }

                mediaPlayer.setDataSource(file?.absolutePath)
                //descriptor.close();
                mediaPlayer.prepare()
                //mediaPlayer.setVolume(0.5f, 0.5f);
                mediaPlayer.setLooping(false)
                data.maxPlayProgress = mediaPlayer.getDuration()
                mediaPlayer.start()
                rotateTheDisk()


                //new Thread(this).start();
            }
            //wasPlaying = false;
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun clearMediaPlayer(mediaPlayer: MediaPlayer) {
        if(mediaPlayer!=null){
            try{
                mediaPlayer.reset()
                mediaPlayer.stop()
                mediaPlayer.release()
            }catch (e:Exception ){
                e.printStackTrace()
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if(this.mediaPlayer!=null)
            clearMediaPlayer(this.mediaPlayer!!)
    }
    private fun rotateTheDisk() {
        try {
            if (!mediaPlayer?.isPlaying()!!)
                return
            image.animate().setDuration(100).rotation(image.getRotation() + 2f).setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    rotateTheDisk()
                    super.onAnimationEnd(animation)
                }
            })
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    private fun emitStatusView(mdata: MessagesData) {
        try {
//            JSONObject jobj = new JSONObject();
            val message = JSONObject()
            message.put(Constants.TAG_USER_ID, GetSet.getUserId())
           /* message.put(Constants.TAG_USER_NAME, GetSet.getUserName())
            message.put(Constants.TAG_MESSAGE_TYPE, mdata.message_type)
            message.put(Constants.TAG_ATTACHMENT, mdata.attachment)
            message.put(Constants.TAG_COMMENT, mdata.message)
            message.put(Constants.TAG_CHAT_TIME, mdata.chat_time)*/
            message.put(Constants.TAG_VIEWED_TIME, mdata.chat_time.toString())
            message.put(Constants.TAG_STATUS_ID, mdata.messageId)
            //            jobj.put(Constants.TAG_SENDER_ID, GetSet.getUserId());
//            jobj.put(Constants.TAG_RECEIVER_ID, userId);
//            jobj.put("message_data", message);
            Log.v("statusViewed", "statusViewed=$message")
            socketConnection!!.statusViewed(message)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
    private fun emitStatusDelete(statusId:String) {
        try {
//            JSONObject jobj = new JSONObject();
            val message = JSONObject()
            message.put(Constants.TAG_USER_ID, GetSet.getUserId())
           /* message.put(Constants.TAG_USER_NAME, GetSet.getUserName())
            message.put(Constants.TAG_MESSAGE_TYPE, mdata.message_type)
            message.put(Constants.TAG_ATTACHMENT, mdata.attachment)
            message.put(Constants.TAG_COMMENT, mdata.message)
            message.put(Constants.TAG_CHAT_TIME, mdata.chat_time)*/
            message.put(Constants.TAG_STATUS_ID, statusId)
            //            jobj.put(Constants.TAG_SENDER_ID, GetSet.getUserId());
//            jobj.put(Constants.TAG_RECEIVER_ID, userId);
//            jobj.put("message_data", message);
            Log.v("statusremove", "statusremove=$message")
            socketConnection!!.statusRemove(message)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
/*    fun getFormattedDate(context: Context?, smsTimeInMilis: Long): String? {
        val smsTime = Calendar.getInstance()
        smsTime.timeInMillis = smsTimeInMilis
        val now = Calendar.getInstance()
        val timeFormatString = "h:mm aa"
        val dateTimeFormatString = "EEE, MMM d h:mm aa"
        val HOURS = 60 * 60 * 60.toLong()
        return if (now[Calendar.DATE] == smsTime[Calendar.DATE]) {
            "Today "+DateFormat.format(timeFormatString, smsTime).toString()
        } else if (now[Calendar.DATE] - smsTime[Calendar.DATE] == 1) {
            getString(R.string.yesterday)
        } else if (now[Calendar.YEAR] == smsTime[Calendar.YEAR]) {
            DateFormat.format(dateTimeFormatString, smsTime).toString()
        } else {
            DateFormat.format("MMM dd yyyy", smsTime).toString()
        }
    }*/

    fun getFormattedDate(context: Context?, smsTimeInMilis: Long): String? {
        val smsTime = Calendar.getInstance()
        smsTime.timeInMillis = smsTimeInMilis
        val now = Calendar.getInstance()
        val timeFormatString = "h:mm aa"
        val dateTimeFormatString = "EEE, MMM d"
        val HOURS = 60 * 60 * 60.toLong()
        return if (now[Calendar.DATE] == smsTime[Calendar.DATE]) {
           "Today "+ DateFormat.format(timeFormatString, smsTime).toString()
        } else if (now[Calendar.DATE] - smsTime[Calendar.DATE] == 1) {
            context!!.getString(R.string.yesterday)+" "+ DateFormat.format(timeFormatString, smsTime).toString()
        } else if (now[Calendar.YEAR] == smsTime[Calendar.YEAR]) {
            DateFormat.format(dateTimeFormatString, smsTime).toString()
        } else {
            DateFormat.format("MMM dd yyyy", smsTime).toString()
        }
    }
}