package com.topzi.chat.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.topzi.chat.BuildConfig;
import com.topzi.chat.R;
import com.topzi.chat.utils.Constants;
import com.topzi.chat.utils.GetSet;
import com.topzi.chat.utils.MyUtils;
import com.topzi.chat.utils.ToastUtils;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import in.myinnos.library.AppIconNameChanger;


public class DisguiseCalculatorActivity extends AppCompatActivity {

    private TextView screen;
    private String display="";
    private EditText inputtext;
    private TextView displaytext,tv_lock_hint;
    private String currentOperator="";
    private String result="";
    boolean isChange=false;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.decoy_mode_ui);
        pref = getSharedPreferences("SavedPref", MODE_PRIVATE);
        editor = pref.edit();
        if(getIntent().hasExtra("isChange"))
            isChange = getIntent().getBooleanExtra("isChange",false);
        ImageButton deletevar = (ImageButton) findViewById(R.id.butdelet);
        deletevar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletenumber();
            }
        });


        screen = (TextView)findViewById(R.id.input_box);
        screen.setText(display);
        inputtext = findViewById(R.id.input_box);
        displaytext = findViewById(R.id.result_box);
        tv_lock_hint=findViewById(R.id.tv_lock_hint);
        if(isChange){
            tv_lock_hint.setVisibility(View.VISIBLE);
            tv_lock_hint.setText("Set calculater PIN and tap + sign to save");
        }else {
            tv_lock_hint.setVisibility(View.GONE);
        }
        String calculaterPin=pref.getString("calculaterPin","");
        inputtext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                //if (etSearchAll.isPerformingCompletion())
                //return;
                if (!isChange){
                    if (calculaterPin.equals(s.toString())){
                        Intent intent = new Intent(DisguiseCalculatorActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                /*if(etSearchCat.getText().length()==0)
                    ivClearSearch.setVisibility(View.GONE);*/
            }
        });

    }

    private void appendToLast(String str) {
        this.inputtext.getText().append(str);
    }

    public void onClickNumber(View v) {
        Button b = (Button) v;
        display += b.getText();
        appendToLast(display);
        display="";
    }

    public void onClickOperator(View v) {
        Button b = (Button) v;
        if(inputtext.getText().length()!=0){
            if(isChange&&b.getText().toString().equals("+")){
                if(inputtext.getText().toString().length()>2){
                    disguiseWarningDialog();
                }else
                    ToastUtils.shortToast("Password is too short");

            }else {
                display += b.getText();
                if(endsWithOperatore())
                {
                    replace(display);
                    currentOperator = b.getText().toString();
                    display = "";
                } else {
                    appendToLast(display);
                    currentOperator = b.getText().toString();

                    display = "";
                }
            }

        }else {
            if(isChange){
                ToastUtils.shortToast(b.getText().toString().equals("+")?"Enter your PIN":"only to use digits 0 to 9");
            }else{
                display += "0"+b.getText();
                if(endsWithOperatore()) {
                    replace(display);
                    currentOperator = b.getText().toString();
                    display = "";
                }else {
                    appendToLast(display);
                    currentOperator = b.getText().toString();

                    display = "";
                }

            }

        }

    }

    private void disguiseWarningDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_disguise_warning);
        dialog.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels * 90 / 100, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

//        TextView title = dialog.findViewById(R.id.title);
        TextView yes = dialog.findViewById(R.id.bt_yes);
        TextView no = dialog.findViewById(R.id.bt_no);

//        yes.setText(getString(R.string.unblock));
//        no.setText(getString(R.string.cancel));
//        title.setText(R.string.unblock_message);

        no.setVisibility(View.VISIBLE);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                editor.putString("calculaterPin", inputtext.getText().toString());
                editor.putString("lockType", "calculator");
                editor.commit();
                ToastUtils.shortToast("Password saved successfully");
//                    MyUtils.changeAppIcon(getApplicationContext(),true);
                String activeName = "com.topzi.chat.activity.MainActivityCalculator";
                List<String> disableNames = new ArrayList<String>();
                disableNames.add("com.topzi.chat.activity.MainActivityApp");
                setAppIcon(activeName, disableNames);
                finish();

            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void setAppIcon(String activeName, List<String> disableNames) {

        new AppIconNameChanger.Builder(DisguiseCalculatorActivity.this)
                .activeName(activeName) // String
                .disableNames(disableNames) // List<String>
                .packageName(BuildConfig.APPLICATION_ID)
                .build()
                .setNow();
    }


    public void onClearButton(View v) {
        inputtext.getText().clear();
        displaytext.setText("");
    }

    public void deletenumber() {
        if(this.inputtext.getText().length()>0)
            this.inputtext.getText().delete(getinput().length() - 1, getinput().length());
    }

    private String getinput() {
        return this.inputtext.getText().length()>0?this.inputtext.getText().toString():"0";
    }

    private boolean endsWithOperatore() {
        return getinput().endsWith("+") || getinput().endsWith("-") || getinput().endsWith("\u00F7") || getinput().endsWith("x");
    }

    private void replace(String str) {
        inputtext.getText().replace(getinput().length() - 1, getinput().length(), str);
    }

    private double operate(String a,String b,String cp)
    {
        switch(cp) {
            case "+": return Double.valueOf(a) + Double.valueOf(b);
            case "-": return Double.valueOf(a) - Double.valueOf(b);
            case "x": return Double.valueOf(a) * Double.valueOf(b);
            case "\u00F7": return Double.valueOf(a) / Double.valueOf(b);
            default: return -1;
        }
    }

    public void equalresult(View v) {
        String input = getinput();

        if(!endsWithOperatore()) {

            if (input.contains("x")) {
                input = input.replaceAll("x", "*");
            }

            if (input.contains("\u00F7")) {
                input = input.replaceAll("\u00F7", "/");
            }

            Expression expression = new ExpressionBuilder(input).build();
            double result = expression.evaluate();

            displaytext.setText(String.valueOf(result));
        }
        else displaytext.setText("");

        System.out.println(result);
    }

}