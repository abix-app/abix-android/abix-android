package com.topzi.chat.activity

import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.topzi.chat.R
import com.topzi.chat.adapters.StaredMessageAdapter
import com.topzi.chat.helper.DatabaseHandler
import com.topzi.chat.helper.NetworkUtil
import com.topzi.chat.model.GroupMessage
import com.topzi.chat.model.MessagesData
import com.topzi.chat.utils.Constants
import com.topzi.chat.utils.GetSet
import com.topzi.chat.utils.MyUtils
import kotlinx.android.synthetic.main.activity_starred_messages.*

class StarredMessagesActivity : AppCompatActivity(), StaredMessageAdapter.OnStaredMessageListener {
    var messagesList: ArrayList<Any> = ArrayList()
    var messagesSelList: ArrayList<Any> = ArrayList()
    var selpos:Int?=-1
    var msgDto:Any?=null
    var dbhelper:DatabaseHandler?=null
    var searchText:String=""
    var madapter:StaredMessageAdapter?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_starred_messages)
        init()
        setListener()
        setAdapter()
    }
    fun init(){
        messagesList.clear()
        dbhelper = DatabaseHandler.getInstance(this)
        try {
            messagesList.addAll(dbhelper?.getStaredMessages(searchText, "0", "20")!!)
            messagesList.addAll(dbhelper?.getStaredGroupMessages(searchText, "0", "20")!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        searchBtnVisibility()
        noItemsVisibility()
    }
    fun searchBtnVisibility(){
        if(messagesList.size>10)
            searchbtn.visibility=View.VISIBLE
        else
            searchbtn.visibility=View.GONE
    }
    fun noItemsVisibility(){
        if(messagesList.size>0){
            no_result_tv.visibility=View.GONE
            rv_stared_messages.visibility=View.VISIBLE
        }else{
            no_result_tv.visibility=View.VISIBLE
            rv_stared_messages.visibility=View.GONE
        }
    }
    fun setListener(){
        backbtn.setOnClickListener { onBackPressed() }
        starBtn.setOnClickListener {
            addStarMes()
        }
        deleteBtn.setOnClickListener { deleteMessageConfirmDialog() }
        copyBtn.setOnClickListener { copyText()  }
        forwordBtn.setOnClickListener { forwardMessage()  }
        searchbtn.setOnClickListener {
            searchView(true)
        }
        cancelbtn.setOnClickListener {
            searchView(false)
        }
        searchView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length > 0) {
                    cancelbtn.visibility = View.VISIBLE
                } else {
                    cancelbtn.visibility = View.GONE
                }
                Constants.searchKey=s.toString().trim()
                messagesList.clear()
                messagesList.addAll(dbhelper?.getStaredMessages(s.toString(), "0", "20")!!)
                messagesList.addAll(dbhelper?.getStaredGroupMessages(s.toString(), "0", "20")!!)
                madapter?.notifyDataSetChanged()
                noItemsVisibility()
//                recyclerViewAdapter.getFilter().filter(s.toString())
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }
    fun searchView(isviewSearch: Boolean){
        if(isviewSearch){
            buttonLayout.visibility=View.GONE
            searchLay.visibility=View.VISIBLE
            cancelbtn.visibility=View.VISIBLE
        }else{
            buttonLayout.visibility=View.VISIBLE
            searchLay.visibility=View.GONE
        }
    }
    fun setAdapter(){
        val gridLayoutManager = GridLayoutManager(this, 1)
        rv_stared_messages.setLayoutManager(gridLayoutManager)
        madapter = dbhelper?.let { StaredMessageAdapter( this,messagesList,this, it) }
        rv_stared_messages.setAdapter(madapter)
    }

    override fun onItemClickListener(msgDto: Any?, position: Int) {
        if(msgDto is MessagesData){
            val i = Intent(this, ChatActivity::class.java)
            if(msgDto.user_id != null &&GetSet.getUserId().equals(msgDto.user_id.toString())){
                i.putExtra("user_id", ""+msgDto.receiver_id)
            }else{
                i.putExtra("user_id", ""+msgDto.sender_id)
            }
            i.putExtra("messageId",msgDto.messageId)
            startActivity(i)
        }else if(msgDto is GroupMessage){
            val intent = Intent(this, GroupChatActivity::class.java)
            intent.putExtra("group_id", msgDto.groupId)
            intent.putExtra("messageId",msgDto.messageId)
            startActivity(intent)
        }
    }

    override fun onLongPressListener(msgDto: Any?, position: Int) {
        selpos=position
        this.msgDto=msgDto
        clearSelection()
        if(msgDto is MessagesData)
            (messagesList.get(position) as MessagesData).isSelected=true
        else if(msgDto is GroupMessage)
            (messagesList.get(position) as GroupMessage).isSelected=true

        madapter?.notifyDataSetChanged()
        forwordLay.visibility=View.VISIBLE
        searchbtn.visibility=View.GONE
        setTopBtnsVisibility()
        starBtnIcons()
        searchView(false)
    }

    fun clearSelection(){
        for (message in messagesList) {
            if(message is MessagesData){
                if((message as MessagesData).isSelected)
                    message.isSelected=false
            }else if(msgDto is GroupMessage){
                if((message as GroupMessage).isSelected)
                    message.isSelected=false
            }
        }
        madapter?.notifyDataSetChanged()
        forwordLay.visibility=View.GONE
        searchBtnVisibility()
        searchView(false)
    }
    fun iscleared(): Boolean {
        for (message in messagesList) {
            if(message is MessagesData){
                if((message as MessagesData).isSelected)
                    return false
            }else if (msgDto is GroupMessage){
                if((message as GroupMessage).isSelected)
                    return false
            }
        }
        return true
    }

    override fun onBackPressed() {
        if(iscleared())
            super.onBackPressed()
        else
            clearSelection()
    }

    private fun addStarMes() {
        if (msgDto is MessagesData) {
            if ((msgDto as MessagesData).isStared .equals("true") ) {
                (msgDto as MessagesData).isStared= "false"
                dbhelper?.updateChatMessage((msgDto as MessagesData).message_id, Constants.TAG_ISSTARED, "false")
            } else {
                (msgDto as MessagesData).isStared = "true"
                dbhelper?.updateChatMessage((msgDto as MessagesData).message_id, Constants.TAG_ISSTARED, "true")
            }
        }else if (msgDto is GroupMessage){
            if ((msgDto as GroupMessage).isStared .equals("true") ) {
                (msgDto as GroupMessage).isStared= "false"
                dbhelper?.updateGroupMessageData((msgDto as GroupMessage).messageId, Constants.TAG_ISSTARED, "false")
            } else {
                (msgDto as GroupMessage).isStared = "true"
                dbhelper?.updateGroupMessageData((msgDto as GroupMessage).messageId, Constants.TAG_ISSTARED, "true")
            }
        }
        init()
        starBtnIcons()
        clearSelection()
        noItemsVisibility()
    }
    fun starBtnIcons() {
        if (msgDto is MessagesData) {
            if ((msgDto as MessagesData).isStared == "true") {
                starBtn.setImageResource(R.drawable.star_cut)
            } else {
                starBtn.setImageResource(R.drawable.ic_round_star_48)
            }
        }else if (msgDto is GroupMessage){
            if ((msgDto as GroupMessage).isStared == "true") {
                starBtn.setImageResource(R.drawable.star_cut)
            } else {
                starBtn.setImageResource(R.drawable.ic_round_star_48)
            }
        }
    }
    fun setTopBtnsVisibility(){
        if(msgDto is MessagesData){
            if((msgDto as MessagesData).message_type.equals("text"))
                copyBtn.visibility=View.VISIBLE
            else
                copyBtn.visibility=View.GONE
        }else if (msgDto is GroupMessage){
            if((msgDto as GroupMessage).messageType.equals("text"))
                copyBtn.visibility=View.VISIBLE
            else
                copyBtn.visibility=View.GONE
        }
    }
    private fun deleteMessageConfirmDialog() {
        val msgList:ArrayList<Any>?= ArrayList()
//        val msgroupList:ArrayList<GroupMessage>?= ArrayList()

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.default_popup)
        dialog.window?.setLayout(resources.displayMetrics.widthPixels * 90 / 100, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        val title = dialog.findViewById<TextView>(R.id.title)
        val yes = dialog.findViewById<TextView>(R.id.yes)
        val no = dialog.findViewById<TextView>(R.id.no)
        yes.text = getString(R.string.im_sure)
        no.text = getString(R.string.nope)
        title.setText(R.string.really_delete_msg)
        no.visibility = View.VISIBLE
        yes.setOnClickListener {
            dialog.dismiss()
            msgList?.clear()
            if(msgDto is MessagesData){
                msgList?.add(msgDto as MessagesData)
                dbhelper?.deleteMessageFromId(msgList as ArrayList<MessagesData>)
            } else if (msgDto is GroupMessage){
                msgList?.add(msgDto as GroupMessage)
                dbhelper?.deleteGroupMessageFromId(msgList as ArrayList<GroupMessage>)
            }

            val msglist: ArrayList<Any> = ArrayList()
            for ((index, value) in messagesList.withIndex()) {
                if(value!=msgDto)
                    msglist.add(value)
            }
            messagesList.clear()
            messagesList.addAll(msglist)
            Toast.makeText(this, getString(R.string.message_deleted), Toast.LENGTH_SHORT).show()
            madapter?.notifyDataSetChanged()
//            chatUserLay.setVisibility(View.VISIBLE)
//            forwordLay.visibility = View.GONE
            clearSelection()
            noItemsVisibility()
        }
        no.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

    fun copyText(){
        var msg: String?=null
        if (msgDto!=null&& msgDto is MessagesData) {
            msg = (msgDto as MessagesData).message
        }else if(msgDto!=null&& msgDto is GroupMessage){
            msg = (msgDto as GroupMessage).message
        }
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Copied Message", msg)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(this, getString(R.string.message_copied), Toast.LENGTH_SHORT).show()
//            madapter?.notifyDataSetChanged()
//            chatUserLay.setVisibility(View.VISIBLE)
//            forwordLay.visibility = View.GONE
        clearSelection()
    }

    fun forwardMessage(){
        if (MyUtils.isNetworkConnected(applicationContext) == NetworkUtil.NOT_CONNECT) {
            Toast.makeText(this,getString(R.string.network_failure),Toast.LENGTH_SHORT).show()
        } else {
            val list:ArrayList<Any> = ArrayList()
            val f = Intent(this, ForwardActivity::class.java)

            if(msgDto is MessagesData){
                f.putExtra("from", "chat")
                list.add(msgDto as MessagesData)
                f.putExtra("data", list as ArrayList<MessagesData>)
            }else if(msgDto!=null&& msgDto is GroupMessage){
                f.putExtra("from", "group")
                list.add(msgDto as GroupMessage)
                f.putExtra("data", list as ArrayList<GroupMessage>)
            }
            startActivityForResult(f, 222)
            clearSelection()
        }
    }

}