package com.topzi.chat.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.animation.DecelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import com.halilibo.bvpkotlin.BetterVideoPlayer
import com.halilibo.bvpkotlin.VideoCallback
import com.topzi.chat.R
import kotlinx.android.synthetic.main.activity_video_player.*

class VideoPlayerActivity : AppCompatActivity(),BetterVideoPlayer.OnVisibilityLister  {
    lateinit var bvp: BetterVideoPlayer
    private val TAG ="VideoPlayerActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_player)
        val intent = intent
        val myUri = intent.getStringExtra("vidUri")
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
        bvp = findViewById(R.id.bvp)!!
        if (savedInstanceState == null) {
            bvp.setAutoPlay(true)
            bvp.setSource(Uri.parse(myUri))
        }
        bvp.setHideControlsOnPlay(true)
        bvp.setToolbarvisibilityLister(this)
        bvp.enableSwipeGestures(window)
        bvp.setCallback(object : VideoCallback {
            override fun onStarted(player: BetterVideoPlayer) {
                Log.i(TAG, "Started")
            }

            override fun onPaused(player: BetterVideoPlayer) {
                Log.i(TAG, "Paused")
            }

            override fun onPreparing(player: BetterVideoPlayer) {
                Log.i(TAG, "Preparing")
            }

            override fun onPrepared(player: BetterVideoPlayer) {
                Log.i(TAG, "Prepared")
            }

            override fun onBuffering(percent: Int) {
                Log.i(TAG, "Buffering $percent")
            }

            override fun onError(player: BetterVideoPlayer, e: Exception) {
                Log.i(TAG, "Error " +e.message)
            }

            override fun onCompletion(player: BetterVideoPlayer) {
                Log.i(TAG, "Completed")
                finish()
            }

            override fun onToggleControls(player: BetterVideoPlayer, isShowing: Boolean) {

            }
        })
        backbtn.setOnClickListener{
            onBackPressed()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            supportActionBar?.hide()
        } else if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            supportActionBar?.show()
        }
    }

    public override fun onPause() {
        bvp.pause()
        super.onPause()
    }
    override fun onTollbarVisibility(isShowing: Boolean) {
        if(isShowing)
            showToolbarWithAnimation()
        else
            hideToolbarWithAnimation()
    }
    private fun hideToolbarWithAnimation() {
        if (toolbar.visibility == View.VISIBLE) {
            toolbar.animate().cancel()
            toolbar.alpha = 1f
            toolbar.visibility = View.GONE
            toolbar.animate().alpha(0f)
                    .setInterpolator(DecelerateInterpolator())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            toolbar.visibility = View.GONE
                            getSupportActionBar()?.hide();
//                            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        }
                    }).start()
        }
    }
    private fun showToolbarWithAnimation(){
        if (toolbar.visibility == View.GONE) {
            toolbar.animate().cancel()
            toolbar.alpha = 0f
            toolbar.visibility = View.VISIBLE
            getSupportActionBar()?.show()
//            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            toolbar.animate().alpha(1f).setListener(null)
                    .setInterpolator(DecelerateInterpolator()).start()
        }
    }
}