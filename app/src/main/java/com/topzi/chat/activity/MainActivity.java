package com.topzi.chat.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.droidninja.imageeditengine.ImageEditor;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.GenericQueryRule;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBNotificationChannel;
import com.quickblox.messages.model.QBSubscription;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.users.model.QBUser;
import com.topzi.chat.R;
import com.topzi.chat.fragments.ChatContainerFragment;
import com.topzi.chat.fragments.SettingsFragment;
import com.topzi.chat.helper.DatabaseHandler;
import com.topzi.chat.helper.QbUsersDbManager;
import com.topzi.chat.helper.SharedPrefManager;
import com.topzi.chat.helper.SocketConnection;
import com.topzi.chat.service.LoginService;
import com.topzi.chat.utils.BottomNavigationViewBehavior;
import com.topzi.chat.utils.Constants;
import com.topzi.chat.utils.GetSet;
import com.topzi.chat.utils.MyUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.READ_CONTACTS;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final String TAG = "MainActivity";
    private static final String TAG_CHATS = "tag_chats";
    private static final String TAG_STATUS = "tag_status";
    public static final String TAG_CALLS = "tag_calls";
    public static final String TAG_SETTINGS = "tag_settings";
    public static final String TAG_DASHBOARD = "tag_dashboard";
    public static BottomNavigationView bottomNavigationView;
    public CircleImageView userImage;
    Toolbar toolbar;
    AppBarLayout appBarLayout;
//    ViewPagerAdapter adapter;
//    TabLayout tabLayout;
//    ViewPager viewPager;
    ImageView navBtn, fab, searchBtn,iv_dashboard;
    DatabaseHandler dbhelper;
    private QbUsersDbManager dbManager;
    public static FrameLayout flAddContainer;
//    DrawerLayout drawer;
//    NavigationView navigationView;
    LinearLayout usrLayout;
    TextView userName;
    StatusFragment statusFragment;
    ChatFragment chatFragment;
    Timer onlineTimer = new Timer();
    String currentVersion="";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String pin = "";
    public static int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 5469;
    private boolean isUpdateShowed=false;

    private static final int PER_PAGE_SIZE_100 = 100;
    private static final String ORDER_RULE = "order";
    private static final String ORDER_DESC_UPDATED = "desc date updated_at";

    Fragment chatsFrag=new ChatContainerFragment();
    Fragment statusFrag=new StatusFragment();
    Fragment callsFrag=new CallFragment();
    Fragment settingsFrag=new SettingsFragment();
    FragmentManager fm = getSupportFragmentManager();
    Fragment active=chatsFrag;

    public static List<Intent> POWERMANAGER_INTENTS = Arrays.asList(
            new Intent().setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")),
            new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity")),
            new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity")),
            new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.startupapp.StartupAppListActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.sysfloatwindow.FloatWindowListActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.floatwindow.FloatWindowListActivity")),
            new Intent().setComponent(new ComponentName("com.oppo.safe", "com.oppo.safe.permission.startup.StartupAppListActivity")),
            new Intent().setComponent(new ComponentName("com.oppo.safe", "com.oppo.safe.permission.floatwindow.FloatWindowListActivity")),
            new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity")),
            new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager")),
            new Intent().setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")),
            new Intent().setComponent(new ComponentName("com.htc.pitroad", "com.htc.pitroad.landingpage.activity.LandingPageActivity")),
            new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.entry.FunctionActivity")).setData(android.net.Uri.parse("mobilemanager://function/entry/AutoStart"))
    );

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.v(TAG, "onCreate");
        if(!isUpdateShowed)
            checkVersion();
       startLoginService();
        dbhelper = DatabaseHandler.getInstance(this);
        dbManager = QbUsersDbManager.getInstance(getApplicationContext());
//        SocketConnection.getInstance(this).setOnUpdateTabIndication(this);
        loadUsers();//load quickblox users
        //initPushManager();
        pref = MainActivity.this.getSharedPreferences("SavedPref", MODE_PRIVATE);
        editor = pref.edit();

        if(Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(MainActivity.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1234);
            }
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        SubscribeService.subscribeToPushes(MainActivity.this, true);
                        QBUser qbUser = sharedPrefsHelper.getQbUser();
                        if(!sharedPrefsHelper.isCreateSubscribe()){
                            sharedPrefsHelper.setSubscribe(true);
                            QBSubscription qbSubscription = new QBSubscription();
                            qbSubscription.setNotificationChannel(QBNotificationChannel.GCM);
                            qbSubscription.setEnvironment(QBEnvironment.DEVELOPMENT);
                            qbSubscription.setRegistrationID(token);
                            qbSubscription.setId(qbUser.getId());
                            QBPushNotifications.createSubscription(qbSubscription).performAsync(new QBEntityCallback<ArrayList<QBSubscription>>() {
                                @Override
                                public void onSuccess(ArrayList<QBSubscription> qbSubscriptions, Bundle bundle) {

                                }

                                @Override
                                public void onError(QBResponseException e) {

                                }
                            });
                        }

                    }
                });

        toolbar = findViewById(R.id.toolbar);
//        viewPager = findViewById(R.id.viewpager);
//        tabLayout = findViewById(R.id.tabs);
        flAddContainer=(FrameLayout)findViewById(R.id.fl_Container);
        bottomNavigationView = (BottomNavigationView)findViewById(R.id.bottom_navigation);
        //setBadge(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(this::onNavigationItemSelected);
//        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) bottomNavigationView.getLayoutParams();
//        layoutParams.setBehavior(new BottomNavigationViewBehavior());
//        navigationView = findViewById(R.id.nav_view);
//        fab = findViewById(R.id.fab);
        searchBtn = findViewById(R.id.searchBtn);
        iv_dashboard=findViewById(R.id.iv_dashboard);
        String dev = SharedPrefManager.getInstance(MainActivity.this).getDeviceToken();
        System.out.println("token " + dev);
        initialiseMainFragments();
        updateFragment(TAG_CHATS, new Bundle());
        if (getIntent().getStringArrayExtra(ImageEditor.EXTRA_EDITED_PATH) != null) {
            updateFragment(TAG_STATUS, new Bundle());
        } else {
            if (getIntent().getStringExtra(Constants.IS_FROM) != null) {
                if (getIntent().getStringExtra(Constants.IS_FROM).equalsIgnoreCase("group")) {
                    updateFragment(TAG_CHATS, new Bundle());
                } else if (getIntent().getStringExtra(Constants.IS_FROM).equalsIgnoreCase("channel")) {
                    updateFragment(TAG_CALLS, new Bundle());
                }
            }
        }

//        View header = navigationView.getHeaderView(0);


//        tabLayout.addOnTabSelectedListener(this);
        searchBtn.setOnClickListener(this);
        iv_dashboard.setOnClickListener(this);
        Glide.with(this).load(Constants.USER_IMG_PATH + GetSet.getImageUrl())
                .apply(new RequestOptions().placeholder(R.drawable.person).error(R.drawable.person))
                .into(iv_dashboard);
//        updateTabIndication();


//        fab.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                if (pref.getString("hideChatPin", "").equals("")) {
//                    setHideChatsPin();
//                } else {
//                    if (!Constants.isShow)
//                        checkHideChatsPin();
//                    else
//                        chatFragment.removeHideData();
//                }
//                return false;
//            }
//        });

//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (tabLayout != null && tabLayout.getSelectedTabPosition() == 0) {
//                    if (ContextCompat.checkSelfPermission(MainActivity.this, READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
//                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{READ_CONTACTS}, 101);
//                    } else {
//                        Intent s = new Intent(getApplicationContext(), SelectContact.class);
//                        s.putExtra(Constants.TAG_USER_ID, GetSet.getUserId());
//                        startActivity(s);
//                    }
//                }
//
//            }
//        });

        startPowerSaverIntent(this);
        startTimer();
    }

    private  void checkVersion(){
        isUpdateShowed=true;
//        try {
//            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
//            Log.e("Current Version","::"+currentVersion);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        new GetVersionCode().execute();
    }
    private void initialiseMainFragments(){
        fm.beginTransaction().add(R.id.fl_Container, settingsFrag, TAG_SETTINGS).hide(settingsFrag).commit();
        fm.beginTransaction().add(R.id.fl_Container, callsFrag, TAG_CALLS).hide(callsFrag).commit();
        fm.beginTransaction().add(R.id.fl_Container, statusFrag, TAG_STATUS).hide(statusFrag).commit();
        fm.beginTransaction().add(R.id.fl_Container, chatsFrag, TAG_CHATS).hide(chatsFrag).commit();
    }

    public void updateFragment(String tag, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        boolean isBackStack = false;
        ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        Fragment fragment = null;
            switch (tag) {

                case TAG_CHATS:
                    fm.beginTransaction().hide(active).show(chatsFrag).commit();
                    active=chatsFrag;
                    active.setArguments(bundle);
                    bottomNavigationView.getMenu().findItem(R.id.action_home).setChecked(true);
                    if (fm.getBackStackEntryCount() > 0)
                        fm.popBackStack();
                    break;
                case TAG_STATUS:
                    flAddContainer.setVisibility(View.VISIBLE);
                    bundle.putBoolean("fromHome", true);
                    fm.beginTransaction().hide(active).show(statusFrag).commit();
                    active=statusFrag;
                    active.setArguments(bundle);
                    //fragment = Fragment.instantiate(this, ShortageProductListFragment.class.getName(), bundle);
                    bottomNavigationView.getMenu().findItem(R.id.action_status).setChecked(true);

                    isBackStack = true;
                    break;
                case TAG_CALLS:
                    flAddContainer.setVisibility(View.VISIBLE);
                    bundle.putBoolean("fromHome", true);
                    fm.beginTransaction().hide(active).show(callsFrag).commit();
                    active=callsFrag;
                    active.setArguments(bundle);
                    //fragment = Fragment.instantiate(this, SurplusProductListFragmentNew.class.getName(), bundle);
                    bottomNavigationView.getMenu().findItem(R.id.action_calls).setChecked(true);

                    isBackStack = true;
                    break;

                case TAG_SETTINGS:
                    flAddContainer.setVisibility(View.VISIBLE);
                    bundle.putBoolean("fromHome",true);
                    fm.beginTransaction().hide(active).show(settingsFrag).commit();
                    active=settingsFrag;
                    //fragment=Fragment.instantiate(this, TraderVirtualStoreContainerFragment.class.getName(),bundle);
                    bottomNavigationView.getMenu().findItem(R.id.action_settings).setChecked(true);
                    isBackStack=true;
                    break;
//                case TAG_DASHBOARD:
//                    flAddContainer.setVisibility(View.VISIBLE);
//                    bundle.putBoolean("fromHome", true);
//                    Constants.searchType = 7;
//                    fm.beginTransaction().hide(active).show(feedFrag).commit();
//                    active=feedFrag;
//                    active.setArguments(bundle);
//                    //fragment = Fragment.instantiate(this, FeedsFragment.class.getName(), bundle);
//                    bottomNavigationView.getMenu().findItem(R.id.action_feed).setChecked(true);
//                    isBackStack = true;
//                    break;
                default:
                    break;

            }


        if (isBackStack) {
            if (fm.getBackStackEntryCount() > 0)
                fm.popBackStack();
            ft.addToBackStack(tag);
        }
//        isBckStack=isBackStack;
//        ft.replace(R.id.fl_Container, fragment, tag);
//        ft.commit();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Bundle bundle = new Bundle();
        switch (item.getItemId()) {
            case R.id.action_home:
//                bundle.putString("productId",id);
                updateFragment(TAG_CHATS,bundle);
                break;
            case R.id.action_status:
                updateFragment(TAG_STATUS,bundle);
                break;
            case R.id.action_calls:
                updateFragment(TAG_CALLS,bundle);
                //setBadge(false);
                break;

            case R.id.action_settings:
                updateFragment(TAG_SETTINGS,bundle);
                break;
        }
        return false;
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }


        @Override

        protected void onPostExecute(String onlineVersion) {

            super.onPostExecute(onlineVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                Log.e("onlineVersion",onlineVersion);
                if (!onlineVersion.equals(currentVersion)&!onlineVersion.equals("Varies with device")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setTitle("Update");
                    alertDialog.setIcon(R.drawable.app_icon);
                    alertDialog.setMessage("New Update is available");

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                            }
                        }
                    });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    alertDialog.show();
                }

            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

        }
    }
    private void setHideChatsPin() {

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialoge_set_pin);
        dialog.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels * 90 / 100, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        TextView title = dialog.findViewById(R.id.title);
        EditText etPin = dialog.findViewById(R.id.etPin);
        TextView yes = dialog.findViewById(R.id.yes);
        TextView no = dialog.findViewById(R.id.no);


        etPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etPin.getText().length() == 4) {
                    if (yes.getText().equals("Confirm")) {
                        if (!etPin.getText().toString().trim().equals(pin)) {

                            etPin.setError("Invalid Pin");
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        no.setVisibility(View.VISIBLE);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("LLLL_Confirm: ", String.valueOf(etPin.getText().length()));
                if (etPin.getText().length() == 4) {
                    if (yes.getText().equals("Confirm")) {
                        if (!etPin.getText().toString().trim().equals(pin)) {
                            etPin.setError("Invalid Pin");
                        } else {
                            editor.putString("hideChatPin", pin);
                            editor.commit();
                            dialog.dismiss();
                        }
                    } else {
                        pin = etPin.getText().toString().trim();
                        etPin.setText("");
                        title.setText("Confirm pin for hide chat");
                        yes.setText("Confirm");
                    }
                } else {
                    etPin.setError("Please Enter 4Digit Pin");
                }
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }



    private void startLoginService() {
        if (sharedPrefsHelper.hasQbUser()) {
            QBUser qbUser = sharedPrefsHelper.getQbUser();
            LoginService.start(this, qbUser);
        }

    }

      /*  QBSessionManager.getInstance().addListener(new QBSessionManager.QBSessionListener() {
            @Override
            public void onSessionCreated(QBSession qbSession) {
                Log.d(TAG, "Session Created");
            }

            @Override
            public void onSessionUpdated(QBSessionParameters qbSessionParameters) {
                Log.d(TAG, "Session Updated");
            }

            @Override
            public void onSessionDeleted() {
                Log.d(TAG, "Session Deleted");
            }

            @Override
            public void onSessionRestored(QBSession qbSession) {
                Log.d(TAG, "Session Restored");
            }

            @Override
            public void onSessionExpired() {
                Log.d(TAG, "Session Expired");
            }

            @Override
            public void onProviderSessionExpired(String provider) {
                Log.d(TAG, "Session Expired for provider:" + provider);
            }
        });
    }*/


//    private void checkHideChatsPin() {
//
//        final Dialog dialog = new Dialog(MainActivity.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setContentView(R.layout.dialoge_set_pin);
//        dialog.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels * 90 / 100, ViewGroup.LayoutParams.WRAP_CONTENT);
//        dialog.setCancelable(true);
//        dialog.setCanceledOnTouchOutside(true);
//
//        EditText etPin = dialog.findViewById(R.id.etPin);
//        TextView title = dialog.findViewById(R.id.title);
//        TextView yes = dialog.findViewById(R.id.yes);
//        TextView no = dialog.findViewById(R.id.no);
//
//        title.setText("Enter the pin");
//
//        no.setVisibility(View.VISIBLE);
//
//        yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!etPin.getText().toString().trim().equals(pref.getString("hideChatPin", ""))) {
//                    etPin.setError("Invalid Pin");
//                } else {
//                    dialog.dismiss();
//                    chatFragment.addHideData();
////                    Intent intent = new Intent(MainActivity.this, HideChatActivity.class);
////                    startActivity(intent);
////                    finish();
//                }
//            }
//        });
//
//        no.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
//
//    }

    private void startTimer() {
        onlineTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                //Function call every second
//                if (!results.blockedme.equals("block") && !results.blockedbyme.equals("block")) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(Constants.TAG_USER_ID, GetSet.getUserId());
                    jsonObject.put(Constants.TAG_CONTACT_ID, GetSet.getUserId());
                    Log.v("online", "online=" + jsonObject);
                    socketConnection.online(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                }
            }
        }, 0, 2000);
    }

    public static void startPowerSaverIntent(Context context) {
        SharedPreferences settings = context.getSharedPreferences("ProtectedApps", Context.MODE_PRIVATE);
        boolean skipMessage = settings.getBoolean("skipProtectedAppCheck", false);
        if (!skipMessage) {
            final SharedPreferences.Editor editor = settings.edit();
            boolean foundCorrectIntent = false;
            for (Intent intent : POWERMANAGER_INTENTS) {
                if (isCallable(context, intent)) {
                    foundCorrectIntent = true;
                    final AppCompatCheckBox dontShowAgain = new AppCompatCheckBox(context);
                    dontShowAgain.setText("Do not show again");
                    dontShowAgain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            editor.putBoolean("skipProtectedAppCheck", isChecked);
                            editor.apply();
                        }
                    });

                    new AlertDialog.Builder(context)
                            .setTitle(Build.MANUFACTURER + " Protected Apps")
                            .setMessage(String.format("%s requires to be enabled in 'Protected Apps' to function properly.%n", context.getString(R.string.app_name)))
                            .setView(dontShowAgain)
                            .setPositiveButton("Go to settings", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        context.startActivity(intent);
                                    } catch (SecurityException se) {
                                        Log.e(TAG, "startPowerSaverIntent: " + se.getMessage());
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.cancel, null)
                            .show();
                    break;
                }
            }
            if (!foundCorrectIntent) {
                editor.putBoolean("skipProtectedAppCheck", true);
                editor.apply();
            }
        }
    }

    private static boolean isCallable(Context context, Intent intent) {
        List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private void enableAutoStart() {
        if (Build.BRAND.equalsIgnoreCase("xiaomi")) {
            new AlertDialog.Builder(MainActivity.this).setTitle("Enable AutoStart")
                    .setMessage(
                            "Please allow AppName to always run in the background,else our services can't be accessed.")
                    .setPositiveButton("ALLOW", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(@NonNull DialogInterface dialog, @NonNull int which) {

                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName("com.miui.securitycenter",
                                    "com.miui.permcenter.autostart.AutoStartManagementActivity"));
                            startActivity(intent);
                        }
                    })
                    .show();
        } else if (Build.BRAND.equalsIgnoreCase("Letv")) {
            new AlertDialog.Builder(MainActivity.this).setTitle("Enable AutoStart")
                    .setMessage(
                            "Please allow AppName to always run in the background,else our services can't be accessed.")
                    .setPositiveButton("ALLOW", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(@NonNull DialogInterface dialog, @NonNull int which) {

                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName("com.letv.android.letvsafe",
                                    "com.letv.android.letvsafe.AutobootManageActivity"));
                            startActivity(intent);
                        }
                    })
                    .show();
        } else if (Build.BRAND.equalsIgnoreCase("Honor")) {
            new AlertDialog.Builder(MainActivity.this).setTitle("Enable AutoStart")
                    .setMessage(
                            "Please allow AppName to always run in the background,else our services can't be accessed.")
                    .setPositiveButton("ALLOW", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(@NonNull DialogInterface dialog, @NonNull int which) {
                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName("com.huawei.systemmanager",
                                    "com.huawei.systemmanager.optimize.process.ProtectActivity"));
                            startActivity(intent);
                        }
                    })
                    .show();
        } else if (Build.MANUFACTURER.equalsIgnoreCase("oppo")) {
            new AlertDialog.Builder(MainActivity.this).setTitle("Enable AutoStart")
                    .setMessage(
                            "Please allow AppName to always run in the background,else our services can't be accessed.")
                    .setPositiveButton("ALLOW", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(@NonNull DialogInterface dialog, @NonNull int which) {
                            try {
                                Intent intent = new Intent();
                                intent.setClassName("com.coloros.safecenter",
                                        "com.coloros.safecenter.permission.startup.StartupAppListActivity");
                                startActivity(intent);
                            } catch (Exception e) {
                                try {
                                    Intent intent = new Intent();
                                    intent.setClassName("com.oppo.safe",
                                            "com.oppo.safe.permission.startup.StartupAppListActivity");
                                    startActivity(intent);
                                } catch (Exception ex) {
                                    try {
                                        Intent intent = new Intent();
                                        intent.setClassName("com.coloros.safecenter",
                                                "com.coloros.safecenter.startupapp.StartupAppListActivity");
                                        startActivity(intent);
                                    } catch (Exception exx) {

                                    }
                                }
                            }
                        }
                    })
                    .show();
        } else if (Build.MANUFACTURER.contains("vivo")) {
            new AlertDialog.Builder(MainActivity.this).setTitle("Enable AutoStart")
                    .setMessage("Please allow AppName to always run in the background.Our app runs in background else our services can't be accesed.")
                    .setPositiveButton("ALLOW", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(@NonNull DialogInterface dialog, @NonNull int which) {
                            try {
                                Intent intent = new Intent();
                                intent.setComponent(new ComponentName("com.iqoo.secure",
                                        "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity"));
                                startActivity(intent);
                            } catch (Exception e) {
                                try {
                                    Intent intent = new Intent();
                                    intent.setComponent(new ComponentName("com.vivo.permissionmanager",
                                            "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
                                    startActivity(intent);
                                } catch (Exception ex) {
                                    try {
                                        Intent intent = new Intent();
                                        intent.setClassName("com.iqoo.secure",
                                                "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager");
                                        startActivity(intent);
                                    } catch (Exception exx) {
                                        ex.printStackTrace();
                                    }
                                }
                            }
                        }
                    })
                    .show();
        }
    }

//    private void updateTabIndication() {
//        for (int i = 0; i < tabLayout.getTabCount(); i++) {
//            TabLayout.Tab tab = tabLayout.getTabAt(i);
//            View selected = tab.getCustomView();
//            TextView indication = selected.findViewById(R.id.indication);
//            ImageView iv_indication=selected.findViewById(R.id.iv_indication);
//
//            if (i == 0) {
//                if (dbhelper.isRecentChatIndicationExist()>0) {
//                    indication.setVisibility(View.VISIBLE);
//                    iv_indication.setVisibility(View.GONE);
//                    indication.setText(""+dbhelper.isRecentChatIndicationExist());
//                } else {
//                    indication.setVisibility(View.GONE);
//                    iv_indication.setVisibility(View.GONE);
//                }
//            } else if (i == 1) {
//                if (dbhelper.isRecentGroupIndicationExist()>0) {
//                    indication.setVisibility(View.VISIBLE);
//                    iv_indication.setVisibility(View.GONE);
//                    indication.setText(""+dbhelper.isRecentGroupIndicationExist());
//                } else {
//                    indication.setVisibility(View.GONE);
//                    iv_indication.setVisibility(View.GONE);
//                }
//            } else if (i == 2) {
//                if (dbhelper.isRecentChannelIndicationExist()>0) {
//                    indication.setVisibility(View.VISIBLE);
//                    iv_indication.setVisibility(View.VISIBLE);
//                    indication.setVisibility(View.GONE);
//                } else {
//                    indication.setVisibility(View.GONE);
//                    iv_indication.setVisibility(View.GONE);
//                }
//            }else
//                indication.setVisibility(View.GONE);
//        }
//    }

    @Override
    public void onNetworkChange(boolean isConnected) {

    }

    public static void setStatusBarGradient(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.gradient);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

//    private void setupViewPager(ViewPager viewPager) {
//        adapter = new ViewPagerAdapter(getSupportFragmentManager(), MainActivity.this);
//        chatFragment = new ChatFragment();
//        adapter.addFragment(chatFragment, getString(R.string.chat));
//        adapter.addFragment(new GroupFragment(), getString(R.string.group));
//        statusFragment = new StatusFragment();
//        adapter.addFragment(statusFragment, getString(R.string.status));
//        adapter.addFragment(new CallFragment(), getString(R.string.calls));
//        viewPager.setAdapter(adapter);
//        tabLayout.setupWithViewPager(viewPager);
//        // Iterate over all tabs and set the custom view
//        for (int i = 0; i < tabLayout.getTabCount(); i++) {
//            TabLayout.Tab tab = tabLayout.getTabAt(i);
//            tab.setCustomView(adapter.getTabView(i, this));
//        }
//        if (getIntent().getStringArrayExtra(ImageEditor.EXTRA_EDITED_PATH) != null) {
//            adapter.setOnSelectView(this, tabLayout, 2);
//        } else
//            adapter.setOnSelectView(this, tabLayout, 0);
//    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.v("requestCode", "requestCode=" + requestCode);
        switch (requestCode) {
            case 101:
                int permContacts = ContextCompat.checkSelfPermission(MainActivity.this,
                        READ_CONTACTS);
                if (permContacts == PackageManager.PERMISSION_GRANTED) {
                    Intent s = new Intent(this, SelectContact.class);
                    s.putExtra(Constants.TAG_USER_ID, GetSet.getUserId());
                    startActivity(s);
                }
                break;
            case Constants.statusCameraImage:
                break;
            case Constants.statusGallery:
                break;
            case Constants.statusAudio:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.navBtn:
//                drawer.openDrawer(Gravity.LEFT);
//                break;
            case R.id.searchBtn:
                startActivity(new Intent(this, SearchActivity.class));
                break;
//            case R.id.usrLayout:
//                Intent p = new Intent(this, ProfileActivity.class);
//                p.putExtra(Constants.TAG_USER_ID, GetSet.getUserId());
//                startActivity(p);
//                drawer.closeDrawer(Gravity.LEFT);
//                break;
            case R.id.iv_dashboard:
//                startActivity(new Intent(this, DashboardActivity.class));
                Intent p = new Intent(this, ProfileActivity.class);
                p.putExtra(Constants.TAG_USER_ID, GetSet.getUserId());
                startActivity(p);
                break;

        }
    }

//    @Override
//    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        Log.v("onNavigation", "=" + item.getTitle());
//        int id = item.getItemId();
//        switch (id) {
////            case R.id.yourrides_menu:
////                Intent channel = new Intent(MainActivity.this, MyChannelsActivity.class);
////                startActivity(channel);
////                break;
//            case R.id.wallet_menu:
//                Intent account = new Intent(MainActivity.this, AccountActivity.class);
//                startActivity(account);
//                break;
//            case R.id.chat_menu:
//                Intent chatsettings = new Intent(MainActivity.this, ChatSettingsActivity.class);
//                startActivity(chatsettings);
//                finish();
//                break;
//            case R.id.chat_notificaton:
//                Intent notificationin = new Intent(MainActivity.this, NotifictionActivity.class);
//                startActivity(notificationin);
//                break;
//            case R.id.data_auto:
//                Intent dataIntent = new Intent(MainActivity.this, DataStorage.class);
//                startActivity(dataIntent);
//                break;
////            case R.id.hide_app:
////                Intent intent = new Intent(MainActivity.this,SignatureVerification.class);
////                startActivity(intent);
//////                PackageManager p = getPackageManager();
//////                ComponentName componentName = new ComponentName(MainActivity.this, BuildConfig.APPLICATION_ID+".activity.SplashActivity");
//////                p.setComponentEnabledSetting(componentName , PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
////                break;
//            case R.id.invite_menu:
//                Intent g = new Intent(Intent.ACTION_SEND);
//                g.setType("text/plain");
//                g.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_message) + "https://play.google.com/store/apps/details?id=" +
//                        getApplicationContext().getPackageName());
//                startActivity(Intent.createChooser(g, "Share"));
//                break;
//            case R.id.help_menu:
//                Intent help = new Intent(MainActivity.this, BaseHelpActivity.class);
//                help.putExtra("fragment_tag", "tag_help");
//                startActivity(help);
//                break;
//            case R.id.stared_msgs:
//                Intent staredMsgs = new Intent(MainActivity.this, StarredMessagesActivity.class);
//                startActivity(staredMsgs);
//                break;
//        }
//        //  switchActivityByNavigation(id, item);
//        drawer.closeDrawer(Gravity.LEFT);
//        return false;
//    }

//    @Override
//    public void onTabSelected(TabLayout.Tab tab) {
//        adapter.setOnSelectView(this, tabLayout, tab.getPosition());
//        if (tab.getPosition() == 0) {
//            fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.home_page_chat));
//            fab.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                    if (drawer != null) {
////                        drawer.closeDrawer(Gravity.LEFT);
////                    }
//                    if (ContextCompat.checkSelfPermission(MainActivity.this, READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
//                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{READ_CONTACTS}, 101);
//                    } else {
//                        Intent s = new Intent(getApplicationContext(), SelectContact.class);
//                        s.putExtra(Constants.TAG_USER_ID, GetSet.getUserId());
//                        startActivity(s);
//                    }
//                }
//            });
//        } else if (tab.getPosition() == 1) {
//            fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.floating_group));
//            fab.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                    if (drawer != null) {
////                        drawer.closeDrawer(Gravity.LEFT);
////                    }
//                    Intent s = new Intent(getApplicationContext(), NewGroupActivity.class);
//                    s.putExtra(Constants.TAG_USER_ID, GetSet.getUserId());
//                    startActivity(s);
//                }
//            });
//        } else if (tab.getPosition() == 2) {
//            fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.floating_channel));
//            fab.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                    if (drawer != null) {
////                        drawer.closeDrawer(Gravity.LEFT);
////                    }
////                    Intent s = new Intent(getApplicationContext(), CreateChannelActivity.class);
////                    s.putExtra(Constants.TAG_USER_ID, GetSet.getUserId());
////                    startActivity(s);
////                    Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
//                    statusFragment.showCreteStatus();
//                }
//            });
//        } else if (tab.getPosition() == 3) {
//            fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.floating_call));
//            fab.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                    if (drawer != null) {
////                        drawer.closeDrawer(Gravity.LEFT);
////                    }
//                    Intent s = new Intent(getApplicationContext(), CallContactActivity.class);
//                    startActivity(s);
//                }
//            });
//        }
//    }

    @Override
    public void onBackPressed() {
//        if (statusFragment.showCreteStatus1())
//            statusFragment.showCreteStatus();
//        else
            super.onBackPressed();
    }

//    @Override
//    public void onTabUnselected(TabLayout.Tab tab) {
//        adapter.setUnSelectView(this, tabLayout, tab.getPosition());
//    }
//
//    @Override
//    public void onTabReselected(TabLayout.Tab tab) {
//
//    }

//    @Override
//    public void updateIndication() {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
////                updateTabIndication();
//            }
//        });
//    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        Context context;

        public ViewPagerAdapter(FragmentManager manager, Context context) {
            super(manager);
            this.context = context;
        }

        public View getTabView(int position, Context context) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
            TextView tabName = (TextView) v.findViewById(R.id.tabName);
            tabName.setText(mFragmentTitleList.get(position));
            tabName.setAllCaps(true);
            // ImageView indication = (ImageView) v.findViewById(R.id.indication);
            return v;
        }

        public void setOnSelectView(Context mContext, TabLayout tabLayout, int position) {
            TabLayout.Tab tab = tabLayout.getTabAt(position);
            View selected = tab.getCustomView();
            TextView tabName = selected.findViewById(R.id.tabName);
            tabName.setTextColor(mContext.getResources().getColor(R.color.white));
        }

        public void setUnSelectView(Context mContext, TabLayout tabLayout, int position) {
            TabLayout.Tab tab = tabLayout.getTabAt(position);
            View selected = tab.getCustomView();
            TextView iv_text = selected.findViewById(R.id.tabName);
            iv_text.setTextColor(mContext.getResources().getColor(R.color.light_end));
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG, "onResume");

        if (LanguageActivity.languageChanged) {
            LanguageActivity.languageChanged = false;
            recreate();
        }
        Log.e(TAG, "onResume: " + GetSet.getUserName());

//        updateTabIndication();
//        userName.setText(GetSet.getUserName());
//        Glide.with(MainActivity.this).load(Constants.USER_IMG_PATH + GetSet.getImageUrl())
//                .apply(new RequestOptions().placeholder(R.drawable.person).error(R.drawable.person))
//                .into(userImage);

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.v(TAG, "onPause");
        SocketConnection.getInstance(this).setOnUpdateTabIndication(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if(statusFragment!=null) {
//            statusFragment.onActivityResult(requestCode, resultCode, data);
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SocketConnection.getInstance(this).setChatCallbackListener(null);
        if (onlineTimer != null) {
            onlineTimer.cancel();
        }
    }

    private void loadUsers() {
        ArrayList<GenericQueryRule> rules = new ArrayList<>();
        rules.add(new GenericQueryRule(ORDER_RULE, ORDER_DESC_UPDATED));

        QBPagedRequestBuilder qbPagedRequestBuilder = new QBPagedRequestBuilder();
        qbPagedRequestBuilder.setRules(rules);
        qbPagedRequestBuilder.setPerPage(PER_PAGE_SIZE_100);

        requestExecutor.loadLastUpdatedUsers(qbPagedRequestBuilder, new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
                Log.e("LLLLLL_Quickbox: ", "Successfully loaded Last 100 created users");
                dbManager.saveAllUsers(qbUsers, true);
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d(TAG, "Error load users" + e.getMessage());
            }
        });
    }
}
