package com.topzi.chat.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import com.topzi.chat.R
import kotlinx.android.synthetic.main.activity_intro.*

class IntroActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
        initviews()
    }
    fun initviews(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            welcome_terms_button.setText(Html.fromHtml(
                    "By <b> Signing up </b>  you agree with our <br>" +
                            "<a href=\"https://topzi.app/terms-of-service.html\">Terms of Service</a> and " +
                            "<a href=\"https://topzi.app/privacy-policy.html\">Privacy Policy</a> ", HtmlCompat.FROM_HTML_MODE_LEGACY))
        }else
            welcome_terms_button.setText(Html.fromHtml("By <b> Signing up </b>  you agree with our <br>" +
                    "<a href=\"https://topzi.app/terms-of-service.html\">Terms of Service</a> and " +
                    "<a href=\"https://topzi.app/privacy-policy.html\">Privacy Policy</a> "))
        welcome_terms_button.setMovementMethod(LinkMovementMethod.getInstance())
        welcome_continue_button.setOnClickListener {
            val i = Intent(this, SigninActivity::class.java)
            startActivity(i)
            finish()
        }
    }

}