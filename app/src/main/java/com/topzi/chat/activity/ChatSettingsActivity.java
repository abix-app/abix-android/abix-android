package com.topzi.chat.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.topzi.chat.R;
import com.topzi.chat.adapters.SolidColorAdapter;
import com.topzi.chat.databinding.ActivityChatSettingsBinding;
import com.topzi.chat.external.AutoCloseBottomSheetBehavior;
import com.topzi.chat.external.ImagePicker;
import com.topzi.chat.helper.DatabaseHandler;
import com.topzi.chat.helper.ImageCompression;
import com.topzi.chat.helper.LocaleManager;
import com.topzi.chat.helper.StorageManager;
import com.topzi.chat.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.topzi.chat.activity.ChatActivity.getPath;

public class ChatSettingsActivity extends AppCompatActivity implements View.OnClickListener, SolidColorAdapter.OnColorSelectedListener {

    String TAG = this.getClass().getSimpleName();
    private ActivityChatSettingsBinding mActivityBinding;

    TextView tvBackup,tvChatHistory;
    Toolbar toolbar;
    ImageView btnBack;
    TextView txtTitle;
    private AutoCloseBottomSheetBehavior<View> bottomDrawerBehavior;
    public static ArrayList<Integer> solidWallpaper = new ArrayList<>();
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    DatabaseHandler dbhelper;
    Dialog dialog1;
    SolidColorAdapter solidColorAdapter;
    StorageManager storageManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat_settings);
        mActivityBinding.setView(this);
        dbhelper = DatabaseHandler.getInstance(this);
        pref = ChatSettingsActivity.this.getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE);
        editor = pref.edit();
        storageManager = StorageManager.getInstance(this);

        toolbar = findViewById(R.id.actionbar);
        btnBack = toolbar.findViewById(R.id.backbtn);
        txtTitle = toolbar.findViewById(R.id.title);
        tvBackup = findViewById(R.id.tv_backup);
        tvChatHistory = findViewById(R.id.tv_chat_history);

        tvChatHistory.setOnClickListener(this);
        tvBackup.setOnClickListener(this);
        setUpBottomDrawer();

        solidWallpaper.clear();
        solidWallpaper.add(R.color.solid_1);
        solidWallpaper.add(R.color.solid_2);
        solidWallpaper.add(R.color.solid_3);
        solidWallpaper.add(R.color.solid_4);
        solidWallpaper.add(R.color.solid_5);
        solidWallpaper.add(R.color.solid_6);
        solidWallpaper.add(R.color.solid_7);
        solidWallpaper.add(R.color.solid_8);
        solidWallpaper.add(R.color.solid_9);
        solidWallpaper.add(R.color.solid_10);
        solidWallpaper.add(R.color.solid_11);
        solidWallpaper.add(R.color.solid_12);
        solidWallpaper.add(R.color.solid_13);
        solidWallpaper.add(R.color.solid_14);
        solidWallpaper.add(R.color.solid_15);
        solidWallpaper.add(R.color.solid_16);
        solidWallpaper.add(R.color.solid_17);
        solidWallpaper.add(R.color.solid_18);
        solidWallpaper.add(R.color.solid_19);
        solidWallpaper.add(R.color.solid_20);
        solidWallpaper.add(R.color.solid_21);

        solidColorAdapter = new SolidColorAdapter(this,solidWallpaper,this);

        mActivityBinding.tvWallpaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDrawerBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
            }
        });

        mActivityBinding.switchEnterIsSend.setChecked(pref.getBoolean(Constants.PREF_ENTER_IS_SEND, false));

        mActivityBinding.switchEnterIsSend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                editor.putBoolean(Constants.PREF_ENTER_IS_SEND, b).commit();
            }
        });

        mActivityBinding.switchMediaVisibility.setChecked(pref.getBoolean(Constants.PREF_MEDIA_VISIBILITY, true));

        mActivityBinding.switchMediaVisibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                editor.putBoolean(Constants.PREF_MEDIA_VISIBILITY, b).commit();
            }
        });

        mActivityBinding.layoutFontSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectFontSize();
            }
        });

        mActivityBinding.layoutAppLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAppLanguage();
            }
        });


        int fontSize = pref.getInt(Constants.PREF_FONT_SIZE, Constants.FONT_SIZE_MEDIUM);

        switch (fontSize) {
            case Constants.FONT_SIZE_SMALL:
                mActivityBinding.textFontSize.setText(getString(R.string.small));
                break;
            case Constants.FONT_SIZE_MEDIUM:
                mActivityBinding.textFontSize.setText(getString(R.string.medium));
                break;
            case Constants.FONT_SIZE_LARGE:
                mActivityBinding.textFontSize.setText(getString(R.string.large));
                break;
        }

        String appLanguage = pref.getString(Constants.TAG_LANGUAGE_CODE, Constants.LANGUAGE_ENGLISH);


        switch (appLanguage) {
            case Constants.LANGUAGE_ENGLISH:
                mActivityBinding.textAppLanguage.setText("English");
                break;
            case Constants.LANGUAGE_HINDI:
                mActivityBinding.textAppLanguage.setText("हिन्दी");
                break;
            case Constants.LANGUAGE_MALAYALAM:
                mActivityBinding.textAppLanguage.setText("മലയാളം");
                break;
            case Constants.LANGUAGE_TAMIL:
                mActivityBinding.textAppLanguage.setText("தமிழ்");
                break;
        }

        mActivityBinding.bottomDrawer.setOnClickListener(this);

        mActivityBinding.layoutNoWallpaper.setOnClickListener(this);

        mActivityBinding.layoutGallery.setOnClickListener(this);
        mActivityBinding.layoutSolidColor.setOnClickListener(this);
        mActivityBinding.layoutRestoreDefault.setOnClickListener(this);
        initToolBar();
    }


    private void initToolBar() {
        txtTitle.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.VISIBLE);
        txtTitle.setText(R.string.chatsettings);
//        btnBack.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.primarytext));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1 && requestCode == 234) {
            try {
                Uri uri = data.getData();
                String picturePath = getPath(ChatSettingsActivity.this, uri);
                Log.e("LLLLLL_Folder: ", picturePath);

                editor.putString("oldWallpaper", picturePath);
                editor.commit();

                Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
                String timestamp = String.valueOf(System.currentTimeMillis() / 1000L);
                String imageStatus = storageManager.saveToSdCard(getApplicationContext(), bitmap, "wallpaper", timestamp + ".jpg");
                if (imageStatus.equals("success")) {
                    File file = storageManager.getImage("wallpaper", timestamp + ".jpg");
                    String filepath = file.getAbsolutePath();
                    editor.putString("newWallpaper", timestamp + ".jpg");
                    editor.putInt("solidColorPos", 0);
                    editor.commit();
                    Log.i(TAG, "selectedImageFile: " + filepath);
//                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                } else {
                    Toast.makeText(this, "13:" + getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.e(TAG, "onActivityResult: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tv_chat_history:
                Intent chathistory = new Intent(ChatSettingsActivity.this,ChatHistoryActivity.class);
                startActivity(chathistory);
                finish();
                break;
            case R.id.tv_backup:
                Intent chatbackup = new Intent(ChatSettingsActivity.this, ChatBackupActivity.class);
                startActivity(chatbackup);
                break;
            case R.id.bottom_drawer:
                break;
            case R.id.layoutRestoreDefault:
            case R.id.layoutNoWallpaper:
                editor.putInt("solidColorPos", 0);
                editor.putString("oldWallpaper", "");
                editor.putString("newWallpaper", "");
                editor.commit();
                bottomDrawerBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                break;
            case R.id.layoutGallery:
                if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, 101);
                } else {
                    ImagePicker.pickImage(this, getString(R.string.select_your_image));
                }
                bottomDrawerBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                break;
            case R.id.layoutSolidColor:
                getSolidColorDialoge();
                bottomDrawerBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                break;
        }
    }

    private void getSolidColorDialoge() {
        dialog1 = new Dialog(ChatSettingsActivity.this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        dialog1.setContentView(R.layout.dialoge_solid_color);
        dialog1.getWindow().setBackgroundDrawableResource(android.R.color.white);
        dialog1.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog1.show();

        ImageView imgBack = dialog1.findViewById(R.id.img_back);
        RecyclerView rvSolidColor = dialog1.findViewById(R.id.rv_solid_color);

        rvSolidColor.setLayoutManager(new GridLayoutManager(ChatSettingsActivity.this, 3));
        rvSolidColor.setAdapter(solidColorAdapter);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog1.isShowing() && dialog1 != null)
                    dialog1.dismiss();
            }
        });
    }

    @Override
    public void onColorSelected(int pos) {
        editor.putInt("solidColorPos", pos + 1);
        editor.putString("oldWallpaper", "");
        editor.putString("newWallpaper", "");
        editor.commit();
//                    imgChatBg.setBackgroundColor(getResources().getColor(str));
        if (dialog1.isShowing() && dialog1 != null)
            dialog1.dismiss();
    }


    private void selectFontSize() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.font_size));
        String[] items = {getString(R.string.small),getString(R.string.medium),getString(R.string.large)};
        int checkedItem = pref.getInt(Constants.PREF_FONT_SIZE, Constants.FONT_SIZE_MEDIUM);
        alertDialog.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Constants.FONT_SIZE_SMALL:
                        mActivityBinding.textFontSize.setText(getString(R.string.small));
                        editor.putInt(Constants.PREF_FONT_SIZE, Constants.FONT_SIZE_SMALL).commit();
                        dialog.dismiss();
                        break;
                    case Constants.FONT_SIZE_MEDIUM:
                        mActivityBinding.textFontSize.setText(getString(R.string.medium));
                        editor.putInt(Constants.PREF_FONT_SIZE, Constants.FONT_SIZE_MEDIUM).commit();
                        dialog.dismiss();
                        break;
                    case Constants.FONT_SIZE_LARGE:
                        mActivityBinding.textFontSize.setText(getString(R.string.large));
                        editor.putInt(Constants.PREF_FONT_SIZE, Constants.FONT_SIZE_LARGE).commit();
                        dialog.dismiss();
                        break;
                }
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }

    private void selectAppLanguage() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.app_language));
        String[] items = {"English","हिन्दी","മലയാളം","தமிழ்"};


        String appLanguage = pref.getString(Constants.TAG_LANGUAGE_CODE, Constants.LANGUAGE_ENGLISH);

        int checkedItem = 0;
        switch (appLanguage) {
            case Constants.LANGUAGE_ENGLISH:
                checkedItem = 0;
                break;
            case Constants.LANGUAGE_HINDI:
                checkedItem = 1;
                break;
            case Constants.LANGUAGE_MALAYALAM:
                checkedItem = 2;
                break;
            case Constants.LANGUAGE_TAMIL:
                checkedItem = 3;
                break;
        }

        alertDialog.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        mActivityBinding.textAppLanguage.setText("English");
                        setNewLocale(Constants.LANGUAGE_ENGLISH, false);
                        dialog.dismiss();
                        break;
                    case 1:
                        mActivityBinding.textAppLanguage.setText("हिन्दी");
                        setNewLocale(Constants.LANGUAGE_HINDI, false);
                        dialog.dismiss();
                        break;
                    case 2:
                        mActivityBinding.textAppLanguage.setText("മലയാളം");
                        setNewLocale(Constants.LANGUAGE_MALAYALAM, false);
                        dialog.dismiss();
                        break;
                    case 3:
                        mActivityBinding.textAppLanguage.setText("தமிழ்");
                        setNewLocale(Constants.LANGUAGE_TAMIL, false);
                        dialog.dismiss();
                        break;
                }
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }

    private boolean setNewLocale(String languageCode, boolean restartProcess) {
        LocaleManager.setNewLocale(this, languageCode);
        editor.putString(Constants.TAG_LANGUAGE_CODE, languageCode).commit();

        LanguageActivity.languageChanged = true;

        recreate();

        if (restartProcess) {
            System.exit(0);
        } else {
//                Toast.makeText(mContext, "Activity restarted", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    protected void setUpBottomDrawer() {
        View bottomDrawer = mActivityBinding.coordinatorLayout.findViewById(R.id.bottom_drawer);
        bottomDrawerBehavior = (AutoCloseBottomSheetBehavior<View>) AutoCloseBottomSheetBehavior.from(bottomDrawer);
        bottomDrawerBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    @Override
    public void onBackPressed() {
        if (dialog1 != null && dialog1.isShowing())
            dialog1.dismiss();

        if (bottomDrawerBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN) {
            bottomDrawerBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
        else {
            finish();
        }
    }

}
