package com.topzi.chat.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.topzi.chat.R
import com.topzi.chat.helper.DatabaseHandler
import com.topzi.chat.helper.DbHelper
import com.topzi.chat.model.ContactsData
import com.topzi.chat.utils.Constants
import com.topzi.chat.utils.GetSet
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {
    var dbhelper: DatabaseHandler? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        dbhelper = DatabaseHandler.getInstance(this)
        initviews()
        initListeners()
    }
    fun initviews(){
        tv_name.setText(GetSet.getUserName())
        Glide.with(this).load(Constants.USER_IMG_PATH + GetSet.getImageUrl())
                .apply(RequestOptions().placeholder(R.drawable.person).error(R.drawable.person))
                .into(iv_ProfileImg)
        val result: ContactsData.Result? = dbhelper?.getContactDetail(GetSet.getUserId().toInt())
        DialogActivity.setAboutUs(result,tv_status)
    }

    fun initListeners(){
        backbtn.setOnClickListener { onBackPressed() }
        lay_top.setOnClickListener {
            val p = Intent(this, ProfileActivity::class.java)
            p.putExtra(Constants.TAG_USER_ID, GetSet.getUserId())
            startActivity(p)
        }
        ll_acount_settings.setOnClickListener {
            var account = Intent(this, AccountActivity::class.java)
            startActivity(account)
        }
        ll_chat.setOnClickListener {
            val chatsettings = Intent(this, ChatSettingsActivity::class.java)
            startActivity(chatsettings)
        }
        ll_notification.setOnClickListener {
            val notificationin = Intent(this, NotifictionActivity::class.java)
            startActivity(notificationin)
        }
        ll_storage.setOnClickListener {
            val dataIntent = Intent(this, DataStorage::class.java)
            startActivity(dataIntent)
        }
        ll_stared.setOnClickListener {
            val staredMsgs = Intent(this, StarredMessagesActivity::class.java)
            startActivity(staredMsgs)
        }
        ll_invite.setOnClickListener {
            val g = Intent(Intent.ACTION_SEND)
            g.type = "text/plain"
            g.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_message) + "https://play.google.com/store/apps/details?id=" +
                    applicationContext.packageName)
            startActivity(Intent.createChooser(g, "Share"))
        }
        ll_help.setOnClickListener {
            val help = Intent(this, BaseHelpActivity::class.java)
            help.putExtra("fragment_tag", "tag_help")
            startActivity(help)
        }
    }
}