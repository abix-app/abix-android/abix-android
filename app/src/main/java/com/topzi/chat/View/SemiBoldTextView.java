package com.topzi.chat.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class SemiBoldTextView extends AppCompatTextView {

    public SemiBoldTextView(Context context) {
        super(context);
        init(context);
    }
    public SemiBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }
    public SemiBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        try
        {
            String font_Name = "font/segui_semi_bold.ttf";
            Typeface font = Typeface.createFromAsset(context.getAssets(), font_Name);
            this.setTypeface(font);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
    }
}
