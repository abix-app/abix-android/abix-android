package com.topzi.chat.adapters

import android.content.Context
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.topzi.chat.R
import com.topzi.chat.activity.ApplicationClass
import com.topzi.chat.helper.DatabaseHandler
import com.topzi.chat.model.ContactsData
import com.topzi.chat.model.GroupMessage
import com.topzi.chat.model.MessagesData
import com.topzi.chat.utils.Constants
import com.topzi.chat.utils.FileUtil
import com.topzi.chat.utils.GetSet
import com.topzi.chat.viewholder.MessageViewHolder
import com.topzi.chat.viewholder.StarFileViewHolder

class StaredMessageAdapter(val context: Context, val messageList: ArrayList<Any>, var mListener: OnStaredMessageListener,val dbhelper: DatabaseHandler) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    val VIEW_TYPE_MESSAGE = 1
    val VIEW_TYPE_IMAGE = 2
    val VIEW_TYPE_VIDEO = 3
    val VIEW_TYPE_LOCATION = 4
    val VIEW_TYPE_CONTACTS = 5
    val VIEW_TYPE_FILE = 6
    val VIEW_TYPE_VOICE = 7

    val VIEW_TYPE_GROUP_MESSAGE = 8
    val VIEW_TYPE_GROUP_IMAGE = 9
    val VIEW_TYPE_GROUP_VIDEO = 10
    val VIEW_TYPE_GROUP_LOCATION = 11
    val VIEW_TYPE_GROUP_CONTACTS = 12
    val VIEW_TYPE_GROUP_FILE = 13
    val VIEW_TYPE_GROUP_VOICE = 14
    override fun getItemViewType(position: Int): Int {
        if(messageList.get(position) is MessagesData){
            val message:MessagesData=messageList.get(position) as MessagesData
            return when (message.message_type) {
                "text" -> VIEW_TYPE_MESSAGE
                "image", "video" -> VIEW_TYPE_IMAGE
                "location" -> VIEW_TYPE_LOCATION
                "contact" -> VIEW_TYPE_CONTACTS
                "voice" -> VIEW_TYPE_VOICE
                else -> VIEW_TYPE_FILE
            }
        }else if(messageList.get(position) is GroupMessage){
            val message:GroupMessage=messageList.get(position) as GroupMessage
            return when (message.messageType) {
                "text" -> VIEW_TYPE_GROUP_MESSAGE
                "image", "video" -> VIEW_TYPE_GROUP_IMAGE
                "location" -> VIEW_TYPE_GROUP_LOCATION
                "contact" -> VIEW_TYPE_GROUP_CONTACTS
                "voice" -> VIEW_TYPE_GROUP_VOICE
                else -> VIEW_TYPE_GROUP_FILE
            }
        }
        return super.getItemViewType(position)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val view= when (viewType) {
            VIEW_TYPE_MESSAGE,VIEW_TYPE_GROUP_MESSAGE -> {
                val v = LayoutInflater.from(context)
                        .inflate(R.layout.item_star_message, null)
                return MessageViewHolder(v)
            }else -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_star_file, null)
                return StarFileViewHolder(view)
            }
        }
        return view
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(messageList.get(position) is MessagesData){
            var message:MessagesData= messageList.get(position) as MessagesData
            val contact: ContactsData.Result=dbhelper.getContactDetail(message.user_id)
            val sendercontact: ContactsData.Result=dbhelper.getContactDetail(message.sender_id)
            val recevercontact: ContactsData.Result=dbhelper.getContactDetail(message.receiver_id)

            if (holder is MessageViewHolder) {
                Glide.with(context).load(Constants.USER_IMG_PATH + contact.user_image).thumbnail(0.5f)
                        .apply(RequestOptions.circleCropTransform()
                                .placeholder(R.drawable.person)
                                .error(R.drawable.person)
                                .override(ApplicationClass.dpToPx(context, 70)))
                        .into(holder.profileImage)

                if(message.user_id != null &&message.user_id.toString().equals(GetSet.getUserId())){

                    holder.incoming_layout_bubble.setBackgroundResource(R.drawable.chat_curved_accent)
                    holder.tvsenderName.text="You"
                    holder.tv_receiverName.text=ApplicationClass.getContactName(context, recevercontact.phone_no)
                }else {
                    holder.incoming_layout_bubble.setBackgroundResource(R.drawable.chat_curved_gradient)
                    holder.tvsenderName.text=ApplicationClass.getContactName(context, sendercontact.phone_no)
                    holder.tv_receiverName.text="You"
                }
//                if(message.sender_id != null &&message.sender_id.equals(GetSet.getUserId())){
//                    holder.tvsenderName.text="You"
//                }else{
//                    holder.tvsenderName.text=ApplicationClass.getContactName(context, contact.phone_no)
//                }

                holder.tv_time_top.text=ApplicationClass.getTime(message.chat_time.toLong())
                holder.text_message_time.text=ApplicationClass.getTime(message.chat_time.toLong())
                val startIndex: Int = message.message.toLowerCase().indexOf(Constants.searchKey.toLowerCase())
                val key: String = message.message.substring(startIndex, startIndex + Constants.searchKey.length)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.text_message_body.text= Html.fromHtml(message.message.replace(key, "<font color='#000000'><b>$key</b></font>"),HtmlCompat.FROM_HTML_MODE_LEGACY)
                }
                holder.ivStar.visibility=View.VISIBLE
                if(message.isSelected)
                    holder.itemView.setBackgroundResource(R.color.black_transparent_50)
                else
                    holder.itemView.setBackgroundResource(R.color.white)
                holder.itemView.setOnClickListener {
                    mListener.onItemClickListener(message,position)
                }
                holder.itemView.setOnLongClickListener {
                    mListener.onLongPressListener(message,position)

                    true
                }
            }else if(holder is StarFileViewHolder){
                holder.progressbar.visibility=View.INVISIBLE
                holder.downloadicon.visibility=View.INVISIBLE
                holder.ivStar.visibility=View.VISIBLE
                Glide.with(context).load(Constants.USER_IMG_PATH + contact.user_image).thumbnail(0.5f)
                        .apply(RequestOptions.circleCropTransform()
                                .placeholder(R.drawable.person)
                                .error(R.drawable.person)
                                .override(ApplicationClass.dpToPx(context, 70)))
                        .into(holder.profileImage)
                when(message.message_type){
                    "video"-> holder.icon.setImageResource(R.drawable.image_placeholder)
                    "file"-> holder.icon.setImageResource(R.drawable.icon_file_unknown)
                    "image"-> holder.icon.setImageResource(R.drawable.image_placeholder)
                    "audio"-> holder.icon.setImageResource(R.drawable.upload_audio)
                }
                holder.icon.setColorFilter(ContextCompat.getColor(context, R.color.bg_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                holder.filename.text=FileUtil.getFileName(message.attachment)
                holder.file_type_tv.text=message.message_type
                holder.tv_time_top.text=ApplicationClass.getTime(message.chat_time.toLong())
                holder.text_message_time.text=ApplicationClass.getTime(message.chat_time.toLong())
                if(message.user_id != null &&message.user_id.toString().equals(GetSet.getUserId())){
                    holder.file_body_lay.setBackgroundResource(R.drawable.chat_curved_accent)
                    holder.tvsenderName.text="You"
                    holder.tv_receiverName.text=ApplicationClass.getContactName(context, recevercontact.phone_no)
                }else {
                    holder.file_body_lay.setBackgroundResource(R.drawable.chat_curved_gradient)
                    holder.tvsenderName.text=ApplicationClass.getContactName(context, sendercontact.phone_no)
                    holder.tv_receiverName.text="You"
                }
                if(message.isSelected)
                    holder.itemView.setBackgroundResource(R.color.black_transparent_50)
                else
                    holder.itemView.setBackgroundResource(R.color.white)
                holder.itemView.setOnClickListener {
                    mListener.onItemClickListener(message,position)
                }
                holder.itemView.setOnLongClickListener {
                    mListener.onLongPressListener(message,position)
                    true
                }
            }
        }else{
            var message:GroupMessage= messageList.get(position) as GroupMessage
            val contact: ContactsData.Result=dbhelper.getContactDetail(message.memberId.toInt())
            if (holder is MessageViewHolder) {
                Glide.with(context).load(Constants.USER_IMG_PATH + contact.user_image).thumbnail(0.5f)
                        .apply(RequestOptions.circleCropTransform()
                                .placeholder(R.drawable.person)
                                .error(R.drawable.person)
                                .override(ApplicationClass.dpToPx(context, 70)))
                        .into(holder.profileImage)

                if(message.memberId != null &&message.memberId.equals(GetSet.getUserId())){

                    holder.incoming_layout_bubble.setBackgroundResource(R.drawable.chat_curved_accent)
                    holder.tvsenderName.text="You"
                    holder.tv_receiverName.text=dbhelper.getGroupData(context,message.groupId).groupName
                }else {
                    holder.incoming_layout_bubble.setBackgroundResource(R.drawable.chat_curved_gradient)
                    holder.tvsenderName.text=dbhelper.getGroupData(context,message.groupId).groupName
                    holder.tv_receiverName.text="You"
                }
//                if(message.sender_id != null &&message.sender_id.equals(GetSet.getUserId())){
//                    holder.tvsenderName.text="You"
//                }else{
//                    holder.tvsenderName.text=ApplicationClass.getContactName(context, contact.phone_no)
//                }

                holder.tv_time_top.text=ApplicationClass.getTime(message.chatTime.toLong())
                holder.text_message_time.text=ApplicationClass.getTime(message.chatTime.toLong())
                val startIndex: Int = message.message.toLowerCase().indexOf(Constants.searchKey.toLowerCase())
                val key: String = message.message.substring(startIndex, startIndex + Constants.searchKey.length)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.text_message_body.text= Html.fromHtml(message.message.replace(key, "<font color='#000000'><b>$key</b></font>"),HtmlCompat.FROM_HTML_MODE_LEGACY)
                }
//                holder.text_message_body.text=message.message
                holder.ivStar.visibility=View.VISIBLE
                if(message.isSelected)
                    holder.itemView.setBackgroundResource(R.color.black_transparent_50)
                else
                    holder.itemView.setBackgroundResource(R.color.white)
                holder.itemView.setOnClickListener {
                    mListener.onItemClickListener(message,position)
                }
                holder.itemView.setOnLongClickListener {
                    mListener.onLongPressListener(message,position)

                    true
                }
            }else if(holder is StarFileViewHolder){
                holder.progressbar.visibility=View.INVISIBLE
                holder.downloadicon.visibility=View.INVISIBLE
                holder.ivStar.visibility=View.VISIBLE
                Glide.with(context).load(Constants.USER_IMG_PATH + contact.user_image).thumbnail(0.5f)
                        .apply(RequestOptions.circleCropTransform()
                                .placeholder(R.drawable.person)
                                .error(R.drawable.person)
                                .override(ApplicationClass.dpToPx(context, 70)))
                        .into(holder.profileImage)
                when(message.messageType){
                    "video"-> holder.icon.setImageResource(R.drawable.image_placeholder)
                    "file"-> holder.icon.setImageResource(R.drawable.icon_file_unknown)
                    "image"-> holder.icon.setImageResource(R.drawable.image_placeholder)
                    "audio"-> holder.icon.setImageResource(R.drawable.upload_audio)
                }
                holder.icon.setColorFilter(ContextCompat.getColor(context, R.color.bg_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
                holder.filename.text=FileUtil.getFileName(message.attachment)
                holder.file_type_tv.text=message.messageType
                holder.tv_time_top.text=ApplicationClass.getTime(message.chatTime.toLong())
                holder.text_message_time.text=ApplicationClass.getTime(message.chatTime.toLong())
                if(message.groupId != null &&message.groupId.equals(GetSet.getUserId())){
                    holder.file_body_lay.setBackgroundResource(R.drawable.chat_curved_accent)
                    holder.tvsenderName.text="You"
                    holder.tv_receiverName.text=message.groupName
                }else {
                    holder.file_body_lay.setBackgroundResource(R.drawable.chat_curved_gradient)
                    holder.tvsenderName.text=message.groupName
                    holder.tv_receiverName.text="You"
                }
                if(message.isSelected)
                    holder.itemView.setBackgroundResource(R.color.black_transparent_50)
                else
                    holder.itemView.setBackgroundResource(R.color.white)
                holder.itemView.setOnClickListener {
                    mListener.onItemClickListener(message,position)
                }
                holder.itemView.setOnLongClickListener {
                    mListener.onLongPressListener(message,position)
                    true
                }
            }
        }
    }

    interface OnStaredMessageListener {
        fun onItemClickListener(msgDto: Any?, position: Int)
        fun onLongPressListener(msgDto: Any?, position: Int)
    }
}