package com.topzi.chat.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.topzi.chat.R
import com.topzi.chat.activity.ApplicationClass
import com.topzi.chat.helper.DatabaseHandler
import com.topzi.chat.model.calls.CallsDataDto
import com.topzi.chat.model.status.StatusViewersDto
import com.topzi.chat.utils.Constants
import com.topzi.chat.utils.MyUtils
import com.topzi.chat.viewholder.status.StatusViewersHolder

class StatusViewersAdapter(val mContext: Context, val viewerList : List<StatusViewersDto>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var dbhelper: DatabaseHandler? = DatabaseHandler.getInstance(mContext);
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(mContext)
                .inflate(R.layout.item_status_viewers, null)
        return StatusViewersHolder(v)
    }

    override fun getItemCount(): Int {
        return viewerList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is StatusViewersHolder){
            val viewerData: StatusViewersDto =viewerList.get(position)
            holder.time.text= MyUtils.getDate(viewerData.viewTime.toLong())
            holder.name.text= ApplicationClass.getContactName(mContext, dbhelper?.getContactDetail(viewerData.userId.toInt())?.phone_no)
            Glide.with(mContext).load(Constants.USER_IMG_PATH + dbhelper?.getContactDetail(viewerData.userId.toInt())?.user_image).thumbnail(0.5f)
                    .apply(RequestOptions.fitCenterTransform().placeholder(R.drawable.person).error(R.drawable.person).override(ApplicationClass.dpToPx(mContext, 70)))
                    .into(holder.profileimage)
        }
    }
}