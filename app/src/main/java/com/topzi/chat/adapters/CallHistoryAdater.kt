package com.topzi.chat.adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.quickblox.users.model.QBUser
import com.quickblox.videochat.webrtc.QBRTCClient
import com.quickblox.videochat.webrtc.QBRTCTypes.QBConferenceType
import com.topzi.chat.R
import com.topzi.chat.activity.ApplicationClass
import com.topzi.chat.activity.CallActivity
import com.topzi.chat.helper.DatabaseHandler
import com.topzi.chat.helper.QbUsersDbManager
import com.topzi.chat.helper.Utils
import com.topzi.chat.model.calls.CallsDataDto
import com.topzi.chat.utils.*
import com.topzi.chat.viewholder.call.CallHistoryViewHolder

class CallHistoryAdater(val mContext: Context, val callList : List<CallsDataDto>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var dbManager = QbUsersDbManager.getInstance(mContext)
    var dbhelper:DatabaseHandler? = DatabaseHandler.getInstance(mContext);
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(mContext)
                .inflate(R.layout.item_call, null)
        return CallHistoryViewHolder(v)
    }

    override fun getItemCount(): Int {
        return callList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is CallHistoryViewHolder){
            val callData:CallsDataDto=callList.get(position)
            if(callData!=null){
                holder.txtTime.text= MyUtils.getDate(callData.createdDate.toLong())
                holder.txtTime.setText(MyUtils.getFormattedDateTime(mContext, callData.createdDate.toLong()))
                when(callData.callType){
                    "incomingaudio","outgoingaudio"->{
                        holder.callType.setImageResource(R.drawable.call)
                    }
                    "incomingvideo","outgoingvideo"->{
                        holder.callType.setImageResource(R.drawable.videocall)
                    }
                }
                if(callData.callType.contains("incoming",false)){
                    when(callData.callStatus){
                        "MISSED","CONNECTING"->{
                            holder.statusIcon.setImageResource(R.drawable.missed)
                        }
                        "CONNECTED","CANCELLED"->{
                            holder.statusIcon.setImageResource(R.drawable.incoming)
                        }
                    }
                }else{
                    when(callData.callStatus){
                        "MISSED","CONNECTED","CANCELLED"->{
                            holder.statusIcon.setImageResource(R.drawable.outgoing)
                        }
                    }
                }
                if(callData.opponetIds.size>0){
                    if(callData.opponetIds.size>1){
                        holder.iv_parentlay.visibility=View.VISIBLE
                        holder.profileImage.visibility=View.GONE
                        var user: QBUser= dbManager.getUserById(callData.opponetIds.get(0).toInt())
                        if(user.login.toInt()==GetSet.getUserId().toInt()){
                            user= dbManager.getUserById(callData.callerId.toInt())
                        }
                        holder.txtName.text = ApplicationClass.getContactName(mContext, dbhelper?.getContactDetail(user.login.toInt())?.phone_no)+" & "+(callData.opponetIds.size-1)+if(callData.opponetIds.size-1<2)" other" else " others"
                        callData.opponetIds.forEachIndexed { index, element ->
                            val user1: QBUser= dbManager.getUserById(callData.opponetIds.get(index).toInt())
                            when(index){
                                0->{
                                    holder.iv_left.visibility=View.VISIBLE
                                    Glide.with(mContext).load(Constants.USER_IMG_PATH + dbhelper?.getContactDetail(user1.login.toInt())?.user_image).thumbnail(0.5f)
                                            .apply(RequestOptions.fitCenterTransform().placeholder(R.drawable.person).error(R.drawable.person).override(ApplicationClass.dpToPx(mContext, 70)))
                                            .into(holder.iv_left)
                                }
                                1->{
                                    holder.iv_top_right.visibility=View.VISIBLE
                                    Glide.with(mContext).load(Constants.USER_IMG_PATH + dbhelper?.getContactDetail(user1.login.toInt())?.user_image).thumbnail(0.5f)
                                            .apply(RequestOptions.fitCenterTransform().placeholder(R.drawable.person).error(R.drawable.person).override(ApplicationClass.dpToPx(mContext, 70)))
                                            .into(holder.iv_top_right)
                                }
                                2->{
                                    holder.iv_bottom_right.visibility=View.VISIBLE
                                    Glide.with(mContext).load(Constants.USER_IMG_PATH + dbhelper?.getContactDetail(user1.login.toInt())?.user_image).thumbnail(0.5f)
                                            .apply(RequestOptions.fitCenterTransform().placeholder(R.drawable.person).error(R.drawable.person).override(ApplicationClass.dpToPx(mContext, 70)))
                                            .into(holder.iv_bottom_right)
                                }
                            }

                        }
                    }else{
                        holder.iv_parentlay.visibility=View.GONE
                        holder.profileImage.visibility=View.VISIBLE
                        if(callData.opponetIds.size>0){
                            var user: QBUser= dbManager.getUserById(callData.opponetIds.get(0).toInt())
                            if(user.login.toInt()==GetSet.getUserId().toInt()&&callData.callerId!=null){
                                user= dbManager.getUserById(callData.callerId.toInt())
                            }
                            holder.txtName.text = ApplicationClass.getContactName(mContext, dbhelper?.getContactDetail(user.login.toInt())?.phone_no)
                            if (Utils.isProfileEnabled(dbhelper?.getContactDetail(user.login.toInt()))) {
                                Glide.with(mContext).load(Constants.USER_IMG_PATH + dbhelper?.getContactDetail(user.login.toInt())?.user_image).thumbnail(0.5f)
                                        .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.person).error(R.drawable.person).override(ApplicationClass.dpToPx(mContext, 70)))
                                        .into(holder.profileImage)
                            } else {
                                Glide.with(mContext).load(R.drawable.person).thumbnail(0.5f)
                                        .transition(DrawableTransitionOptions().crossFade())
                                        .into(holder.profileImage)
                            }
                        }

                    }
                }
                holder.callType.setOnClickListener {
                    var currentOpponentsList = dbManager.allUsers
                    Constants.qbUsersList.clear()
                    for (i in currentOpponentsList.indices) {
                        val qbUser = currentOpponentsList[i]
                        for (j in callData.opponetIds.indices) {
                            var user: QBUser= dbManager.getUserById(callData.opponetIds.get(j).toInt())
                            if(user.login.toInt()==GetSet.getUserId().toInt()){
                                user= dbManager.getUserById(callData.callerId.toInt())
                            }
                            if (qbUser.login == user.login)
                                Constants.qbUsersList.add(qbUser)
                        }

                    }
                    when(callData.callType){
                        "incomingaudio","outgoingaudio"->{
                            if (MyUtils.isOnline(mContext as Activity?))
                                startCall(false)
                        }
                        "incomingvideo","outgoingvideo"->{
                            if (MyUtils.isOnline(mContext as Activity?))
                                startCall(true)
                        }
                    }
                }
            }

        }
    }
    private fun startCall(isVideoCall: Boolean) {
        if (Constants.qbUsersList.size > Constants.MAX_OPPONENTS_COUNT) {
            ToastUtils.longToast(String.format(mContext.getString(R.string.error_max_opponents_count),
                    Constants.MAX_OPPONENTS_COUNT))
            return
        }
        val opponentsList = CollectionsUtils.getIdsSelectedOpponents(Constants.qbUsersList)
        if(opponentsList.size>0){
            val conferenceType = if (isVideoCall) QBConferenceType.QB_CONFERENCE_TYPE_VIDEO else QBConferenceType.QB_CONFERENCE_TYPE_AUDIO
            val qbrtcClient = QBRTCClient.getInstance(mContext)
            val newQbRtcSession = qbrtcClient.createNewSessionWithOpponents(opponentsList, conferenceType)
            WebRtcSessionManager.getInstance(mContext).currentSession = newQbRtcSession
            PushNotificationSender.sendPushMessage(opponentsList, GetSet.getUserName())
            CallActivity.start(mContext, false)
        }

    }

}