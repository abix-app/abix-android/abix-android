package com.topzi.chat.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.topzi.chat.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SolidColorAdapter extends RecyclerView.Adapter<SolidColorAdapter.MyClassView> {
    public static ArrayList<Integer> solidWallpaperist;
    OnColorSelectedListener mListener;
    Context mContext;
    public SolidColorAdapter(Context context,ArrayList<Integer> solidWallpaper,OnColorSelectedListener listener) {
        this.solidWallpaperist=solidWallpaper;
        mListener=listener;
        mContext=context;
    }

    @NonNull
    @Override
    public SolidColorAdapter.MyClassView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_solid_color, parent, false);
        return new SolidColorAdapter.MyClassView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SolidColorAdapter.MyClassView holder, int position) {

        Integer str = solidWallpaperist.get(position);
        Log.e("LLLLLL_solid", String.valueOf(str));
        holder.imgSolidColor.setColorFilter(mContext.getResources().getColor(str), PorterDuff.Mode.SRC_IN);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onColorSelected(position);
            }

        });
    }

    @Override
    public int getItemCount() {
        return solidWallpaperist.size();
    }

    public class MyClassView extends RecyclerView.ViewHolder {
        ImageView imgSolidColor;

        public MyClassView(@NonNull View itemView) {
            super(itemView);

            imgSolidColor = itemView.findViewById(R.id.img_solid_Color);
        }
    }
    public interface OnColorSelectedListener{
        void onColorSelected(int pos);
    }
}