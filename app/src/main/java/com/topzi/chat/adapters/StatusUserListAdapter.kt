package com.topzi.chat.adapters

import android.content.Context
import android.content.Intent
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.topzi.chat.R
import com.topzi.chat.activity.ApplicationClass
import com.topzi.chat.activity.DialogActivity
import com.topzi.chat.activity.StatusStoriesActivity
import com.topzi.chat.activity.StatusStoriesActivitynew
import com.topzi.chat.helper.DatabaseHandler
import com.topzi.chat.helper.ForegroundService
import com.topzi.chat.model.status.BaseStatusDto
import com.topzi.chat.utils.Constants
import com.topzi.chat.utils.GetSet
import com.topzi.chat.viewholder.status.HeaderViewHolder
import com.topzi.chat.viewholder.status.StatusUserHolder
import java.util.*
import kotlin.collections.HashMap

class StatusUserListAdapter(val context: Context, val mainList: ArrayList<BaseStatusDto>, var mListener: OnStatusListener, val dbhelper: DatabaseHandler): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val VIEW_TYPE_HEADER = 1
    val VIEW_TYPE_USER = 2
    override fun getItemViewType(position: Int): Int {
        return when (mainList.get(position).type) {
            "header" -> VIEW_TYPE_HEADER
            else -> VIEW_TYPE_USER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view= when (viewType) {
            VIEW_TYPE_HEADER -> {
                val v = LayoutInflater.from(context)
                        .inflate(R.layout.status_header_item, null)
                return HeaderViewHolder(v)
            }else -> {
                val view = LayoutInflater.from(context).inflate(R.layout.status_user_item, null)
                return StatusUserHolder(view)
            }
        }
        return view
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is StatusUserHolder) {
            if(mainList.get(position).status!=null){
                val hashMap: HashMap<String, String> = mainList.get(position).status
                if (hashMap[Constants.TAG_SENDER_ID] == GetSet.getUserId()){
                    holder.profileview.visibility=View.GONE
                    holder.tv_name.setText(Constants.myStatus)
                    holder.menu_details.visibility=View.GONE
                } else{
                    holder.tv_name.setText(hashMap[Constants.TAG_USER_NAME])
                    holder.menu_details.visibility=View.GONE
                }
                if(hashMap[Constants.TAG_CHAT_TIME]!=null)
                    holder.tv_time.setText(getFormattedDateStatus(context,hashMap[Constants.TAG_CHAT_TIME]?.replace(".0", "")?.toLong()!!))
                if (hashMap[Constants.TAG_BLOCKED_ME] == "block") {
                    Glide.with(context).load(R.drawable.change_camera).thumbnail(0.5f)
                            .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.change_camera).error(R.drawable.change_camera).override(ApplicationClass.dpToPx(context, 70)))
                            .into(holder.profileimage)
                } else {
                    DialogActivity.setProfileImage(dbhelper.getContactDetail(hashMap[Constants.TAG_USER_ID]?.toInt()), holder.profileimage, context)
                }


//            if (hashMap.get(Constants.TAG_CHANNEL_TYPE).equalsIgnoreCase(Constants.TAG_PRIVATE)) {
//                holder.privateImage.setVisibility(View.VISIBLE);
//            } else {
//                holder.privateImage.setVisibility(View.GONE);
//            }
                val allStatus = dbhelper.getAllStatus(hashMap[Constants.TAG_USER_ID], "0");
                val unreadCount = dbhelper.getUnseenStatusCount(hashMap[Constants.TAG_USER_ID]?.toInt())
                holder.circular_status_view.setPortionsCount(allStatus.size)
                holder.circular_status_view.setPortionsColor(context.getResources().getColor(R.color.emoji_gray_portion));

                if(!hashMap[Constants.TAG_USER_ID]!!.equals(GetSet.getUserId())) {
                    if (allStatus.size > 0) {
                        Log.v("Status viewed0", hashMap[Constants.TAG_USER_ID]+"")

                        var allseen = true;
                        var pos = 0
                        for (messageData in allStatus) {
                            Log.v("Status viewed1", messageData.isViewed+" "+hashMap[Constants.TAG_USER_ID]!!)
                            if (messageData.isViewed != null) {
                                if (messageData.isViewed.equals("no")) {
                                    holder.circular_status_view.setPortionColorForIndex(pos, context.getResources().getColor(R.color.blue))
                                    allseen = false;
                                } else {
                                    Log.v("Status viewed3", messageData.isViewed + " " + hashMap[Constants.TAG_USER_ID]!!)
                                    holder.circular_status_view.setPortionColorForIndex(pos, context.getResources().getColor(R.color.emoji_gray_portion))

                                    //  holder.circular_status_view.invalidate()
                                    //    holder.circular_status_view.setPortionColorForIndex(pos,0)
                                    //holder.circular_status_view.setPortionsColor( R.color.emoji_gray_portion)

                                    //holder.circular_status_view.setPortionColorForIndex(pos, R.color.emoji_gray_portion)

                                    //holder.circular_status_view.setPortionColorForIndex(pos,   R.color.status_default_color)

                                }
                            }
                            else{
                                holder.circular_status_view.setPortionColorForIndex(pos, context.getResources().getColor(R.color.emoji_gray_portion))

                            }
                            pos++
                        }
                        if(allseen)
                        {
                            holder.circular_status_view.setPortionsColor(context.getResources().getColor(R.color.emoji_gray_portion));

                        }
                    }
                }


//            if (unreadCount > 0 && hashMap[Constants.TAG_SENDER_ID] != GetSet.getUserId()) {
////                holder.unseenLay.setVisibility(View.VISIBLE)
////                holder.unseenCount.setText("" + unreadCount)
////                holder.circular_status_view.setPortionsCount(allStatus.size)
//            } else {
//
//                holder.unseenLay.setVisibility(View.GONE)
//            }
                holder.itemView.setOnClickListener {
                    mListener.onStatusClickListener(hashMap,position,mainList)
                }
            }else{
                Glide.with(context).load(Constants.USER_IMG_PATH + GetSet.getImageUrl())
                        .apply(RequestOptions().placeholder(R.drawable.person).error(R.drawable.person))
                        .into(holder.profileimage)
                val allStatus = dbhelper.getAllStatus(GetSet.getUserId(), "0");
                holder.circular_status_view.setPortionsCount(allStatus.size)
                if(allStatus.size>0)
                    holder.menu_details.visibility=View.GONE
                else
                    holder.menu_details.visibility=View.GONE
                holder.tv_name.setText(Constants.myStatus)
                holder.profileview.visibility=View.GONE
                holder.tv_time.text="Tap to add status update"
                holder.itemView.setOnClickListener {
                    if(allStatus.size>0){
                        var status: HashMap<String, String> = HashMap();
                        status.put(Constants.TAG_USER_NAME,GetSet.getUserName())
                        status.put(Constants.TAG_USER_ID,GetSet.getUserId())
                        val intent = Intent(context, StatusStoriesActivitynew::class.java)
                        intent.putExtra("sender_id", GetSet.getUserId())
                        intent.putExtra("status", status)
                        context.startActivity(intent)
                    }else
                        mListener.onAddMystatusListener()
                }
            }


        }else if(holder is HeaderViewHolder){
            holder.tv_header.text=mainList.get(position).title
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
    }

    interface OnStatusListener {
        fun onStatusClickListener(status: HashMap<String, String>,position:Int,mainList: ArrayList<BaseStatusDto>)
        fun onAddMystatusListener();
    }
    fun getFormattedDateStatus(context: Context?, smsTimeInMilis: Long): String? {
        val smsTime = Calendar.getInstance()
        smsTime.timeInMillis = smsTimeInMilis
        val now = Calendar.getInstance()
        val timeFormatString = "h:mm aa"
        val dateTimeFormatString = "EEE, MMM d"
        val HOURS = 60 * 60 * 60.toLong()
        return if (now[Calendar.DATE] == smsTime[Calendar.DATE]) {
            "Today "+ DateFormat.format(timeFormatString, smsTime).toString()
        } else if (now[Calendar.DATE] - smsTime[Calendar.DATE] == 1) {
            context!!.getString(R.string.yesterday)+" "+ DateFormat.format(timeFormatString, smsTime).toString()
        } else if (now[Calendar.YEAR] == smsTime[Calendar.YEAR]) {
            DateFormat.format(dateTimeFormatString, smsTime).toString()
        } else {
            DateFormat.format("MMM dd yyyy", smsTime).toString()
        }
    }
}