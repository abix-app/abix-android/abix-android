package com.topzi.chat.model.calls;

public enum CallStatus {
    CALL_STATE_RIGING,
    CALL_STATE_CANCELD,
    CALL_STATE_REJECTED,
    CALL_STATE_ACCEPTED,
    CALL_STATE_NO_ANSWER
}
