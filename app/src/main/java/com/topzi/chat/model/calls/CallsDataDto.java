package com.topzi.chat.model.calls;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CallsDataDto {
    public String sessionId;
    public String callerId;
    public String callStatus;
    public String callDuration;
    public String callType;
    public String createdDate;
    public List<String> opponetIds;
}
