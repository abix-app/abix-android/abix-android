package com.topzi.chat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class GroupChatResult {
    @SerializedName("status")
    public Boolean status;
    @SerializedName("result")
    public List<GroupMessage> result;

}
