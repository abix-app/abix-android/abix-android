package com.topzi.chat.model.status;

import com.google.gson.annotations.SerializedName;

import java.time.OffsetDateTime;
import java.util.List;

public class RecentStatusDto {
    @SerializedName("STATUS")
    public Boolean status;
    @SerializedName("MSG")
    public String msg;
    @SerializedName("DATA")
    public List<Datum> data;

    public class Datum {
        @SerializedName("userId")
        public String userID;
        public List<Status> status;
    }
    public class Status {
        public Long id;
        public String referenceId;
        @SerializedName("userId")
        public Long userID;
        public String filePath;
        public String comment;
        public String type;
        public String expiresOn;
        public Boolean isDeleted;
        public String createdAt;
        public String updatedAt;
    }
}
