package com.topzi.chat.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 5/7/18.
 */

public class RecentsData {

    @SerializedName("status")
    public Boolean status;

    @SerializedName("result")
    public List<MessagesData> result;
}
