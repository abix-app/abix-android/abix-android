package com.topzi.chat.model.status;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class BaseStatusDto implements Serializable {
    public String type;
    public String title;
    public HashMap<String, String> status;
}
