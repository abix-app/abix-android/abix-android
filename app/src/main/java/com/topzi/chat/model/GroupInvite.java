package com.topzi.chat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupInvite {
    @SerializedName("status")
    public Boolean status;
    @SerializedName("result")
    public List<GroupData> result;
}
