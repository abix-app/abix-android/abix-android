package com.topzi.chat.model.status;

public class StatusViewersDto {
    public String userId;
    public String statusId;
    public String viewTime;
}
