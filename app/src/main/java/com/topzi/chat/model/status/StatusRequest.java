package com.topzi.chat.model.status;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StatusRequest {
    @SerializedName("userId")
    public List<Integer> userIds;
}
